/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  safelist: [
    {pattern: /bg-./},
  ],
  theme: {
    extend: {
      colors: {
        mainColor: "#EBD303",
        whiteShirt: "#ffff",
        yellowDarkShirt: "#ffc314",
        blackShirt: "#000000ba",
        redShirt: "#fb1227",
        orangeShirt: "#fe5311",
        greenShirt: "#229c54",
        blueShirt: "#26489e",
        pinkShirt: "#faa1c0",
        yellowShirt: "#fbfd19",
        redDarkShirt: "#4e082f",
        blueSkyShirt: "#84b8d7",
        azureBlue: "#066b96",
        purpleShirt: "#2e0658",
        purpleDarkShirt: "#061238",
        limeShirt: "#6cbc38",
        grayShirt: "#d2d7d6",
        rgbRed: "rgb(195, 21, 58)",
      },
      screens: {
        "2xl": "1536px",
      },
    },
  },
  plugins: [],
}