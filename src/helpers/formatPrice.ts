export function formatPrice(price: number, currency?: string) {
    let fixedPrice = Number(price).toFixed(2);
    const currencyFormatter = new Intl.NumberFormat("de-DE", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
        ...(currency && {currency}),
        ...(currency && {style: "currency"}),
    });
    return currencyFormatter.format(Number(fixedPrice));
}
