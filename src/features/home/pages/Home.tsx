import {TopBg} from "../components/TopBg";
import {Services} from "../components/Services";
import {Footer} from "../components/Footer";
import {useEffect} from "react";
export const Home = () => {

    useEffect(() => {
        document.title = `Custom Lab | Home`;
    }, []);

    return (
        <>
            <TopBg />
            <Services />
            <Footer />
        </>
    )
}