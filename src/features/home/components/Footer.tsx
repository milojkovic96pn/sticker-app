import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import laptopMockuper from "../../../assets/images/mockuperLaptop.png"
import phoneMockuper from "../../../assets/images/mockuperPhone.png"

export const Footer = () => {

    return (
        <footer className="relative bg-mainColor pt-8 pb-6">
            <div className="box"></div>
            <div className=" mx-auto px-4">
                <div className="flex flex-wrap">
                    <div className={"md:hidden flex mb-7"}>
                        <div className="w-3/12 flex items-end">
                            <img src={phoneMockuper} alt={"phone-mockuper"} className={"object-scale-down"} />
                        </div>
                        <div className="w-9/12">
                            <img src={laptopMockuper} alt={"laptop-mockuper"} />
                        </div>
                    </div>
                    <div className="w-full lg:w-3/12 pl-7 text-center lg:text-left pb-5 lg:pb-0">
                        <h4 className="text-3xl font-semibold">Let's keep in touch!</h4>
                        <h5 className="text-lg mt-0 mb-2 text-gray-700">
                            Find us on any of these platforms, we respond 1-2 business days.
                        </h5>
                        <div className="lg:mt-6 text-center lg:text-left">
                            <button
                                className="bg-white text-blue-400 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                                type="button"
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mx-auto h-full w-4"
                                    fill="currentColor"
                                    viewBox="0 0 24 24">
                                    <path
                                        d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z"/>
                                </svg>
                            </button
                            >
                            <button
                                className="bg-white text-blue-600 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                                type={"button"}
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="mx-auto h-full w-4"
                                    fill="currentColor"
                                    viewBox="0 0 24 24">
                                    <path
                                        d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div className="w-full lg:w-3/12 2xl:w-4/12 px-4 text-center lg:text-left pb-5 lg:pb-0 relative hidden md:flex">
                        <img src={laptopMockuper} alt={"laptop-mockuper"} className={"lg:absolute lg:-top-[150px] 2xl:-top-[250px] left-0"}/>
                    </div>
                    <div className="w-full lg:w-4/12 2xl:w-3/12 px-4 text-center lg:text-left">
                        <div className="flex flex-wrap items-top mb-6">
                            <div className="w-full md:w-3/6 px-4 ml-auto pt-5 pb-5 lg:pt-0 lg:pb-0">
                                <span
                                    className="block uppercase text-gray-600 text-sm font-semibold mb-2"
                                >
                                    Useful Links
                                </span>
                                <ul className="list-unstyled">
                                    <li>
                                        <span
                                            className="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                        >
                                            Home
                                        </span>
                                    </li>
                                    <li>
                                        <span
                                            className="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                        >
                                            About
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div className="w-full md:w-3/6 px-4 text-[15px]">
                                <span
                                    className="block uppercase text-gray-600 text-sm font-semibold mb-2 text-center lg:text-left"
                                >
                                    Other Information
                                </span>
                                <p className="mb-4 flex items-center justify-center md:justify-start">
                                    <FontAwesomeIcon
                                        icon={["fas", "location-dot"]}
                                        className="mr-3 h-5 w-5"
                                    />
                                    Polje 21, IZOLA
                                </p>
                                <p className="mb-4 flex items-center justify-center md:justify-start">
                                    <FontAwesomeIcon
                                        icon={["fas", "envelope"]}
                                        className="mr-3 h-5 w-5"
                                    />
                                    tiskizsek@gmail.com
                                </p>
                                <p className="mb-4 flex items-center justify-center md:justify-start">
                                    <FontAwesomeIcon
                                        icon={["fas", "phone"]}
                                        className="mr-3 h-5 w-5"
                                    />
                                    +386 68-601-440
                                </p>
                                <p className="mb-4 flex items-center justify-center md:justify-start">
                                    <FontAwesomeIcon
                                        icon={["fas", "clock"]}
                                        className="mr-3 h-5 w-5"
                                    />
                                    7.30am – 16.30pm
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="w-full lg:w-1/12 2xl:w-2/12 px-4 text-center lg:text-left pb-5 lg:pb-0 relative justify-center hidden md:flex">
                        <img src={phoneMockuper} alt={"phone-mockuper"} className={"lg:absolute lg:-top-[150px] 2xl:-top-[200px] right-10 w-[160px] left-12"} />
                    </div>
                </div>
                <hr className="my-2 border-gray-400"/>
                <div
                    className="flex flex-wrap items-center md:justify-between justify-center"
                >
                    <div className="w-full md:w-4/12 px-4 mx-auto text-center">
                        <div className="text-sm text-gray-600 font-semibold py-1">
                            Copyright © 2023 {" "}
                            <span
                                className="text-gray-600 hover:text-gray-900"
                            >
                                Marko Tech
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}