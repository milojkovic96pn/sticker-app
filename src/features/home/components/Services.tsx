import tShirt from "../../../assets/images/t-shirt.jpg"
import stickers from "../../../assets/images/stickers.png"
import {VideoInfo} from "./VideoInfo";
import {useNavigate} from "react-router-dom";
export const Services = () => {

    const navigate = useNavigate()

    return (
        <section className="md:pb-20 bg-[#d8d8d8] pt-[5rem]">
            <div className="mx-auto lg:px-4">
                <div className="flex flex-wrap">
                    <div className="w-full md:w-6/12 px-2 text-center order-2 md:order-1 2xl:pl-[9rem]">
                        <div
                            className="relative flex items-center min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg h-[100%]"
                        >
                            <div className="px-4 py-5 flex-auto lg:flex justify-center items-center">
                                <div
                                    className="text-white p-3 text-center inline-flex items-center justify-center lg:w-[300px]"
                                >
                                    <img src={tShirt} alt={"custom-shirt"}/>
                                </div>
                               <div>
                                   <h6 className="text-3xl font-semibold text-ComingSoon">Custom t-shirts</h6>
                                   <p className="mt-2 mb-4 text-gray-600 text-ComingSoon italic">
                                       Discover the perfect blend of style and comfort. <br/>
                                       Our product, made from 100% renowned ring-spun cotton
                                   </p>
                                   <span
                                       className="cursor-pointer bg-mainColor text-gray-800 active:bg-gray-100 text-xs font-bold uppercase px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none lg:mr-1 lg:mb-0 ml-3 mb-3"
                                       onClick={() => navigate("/design-shirt")}
                                   >
                                     Design Now
                                   </span>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div className="w-full md:w-6/12 px-2 text-center order-1 md:order-2  mb-8 md:mb-0 2xl:pr-[9rem]">
                        <div
                            className="relative flex items-center min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg h-[100%]"
                        >
                            <div className="px-4 py-5 flex-auto lg:flex justify-center items-center">
                                <div
                                    className="text-white p-3 text-center inline-flex items-center justify-center"
                                >
                                    <img src={stickers} alt={"custom-stickers"} className={"lg:max-w-[500px] w-[100%]"} />
                                </div>
                                <div>
                                    <h6 className="text-xl font-semibold text-ComingSoon">Custom Stickers</h6>
                                    <p className="mt-2 mb-4 text-gray-600 text-ComingSoon italic">
                                        Pride in top-quality stickers. Durable, vibrant, <br />
                                        and fade-resistant for all your adventures!
                                    </p>
                                    <span
                                        className="cursor-pointer bg-mainColor text-gray-800 active:bg-gray-100 text-xs font-bold uppercase px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none lg:mr-1 lg:mb-0 ml-3 mb-3"
                                        onClick={() => navigate("/design-sticker")}
                                    >
                                       Design Now
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <VideoInfo />
            </div>
        </section>
    )
}