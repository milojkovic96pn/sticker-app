export const VideoInfo = () => {

    return (
        <div className="flex flex-wrap items-center lg:mt-32 2xl:pb-[15rem] lg:pb-[5rem] pt-10 lg:pt-0 px-2 2xl:pl-[9rem] lg:pl-[2rem]">
            <div className="w-full lg:w-6/12 px-2 text-center order-2 md:order-1 2xl:pl-[9rem]">
                <h2 className="md:text-2xl 2xl:text-4xl mb-2 font-semibold leading-normal text-[18px] text-center md:text-left">
                    Shipping is free on all orders over 20€,<br/>
                    free online proofs, fast turnaround.
                </h2>
                <p
                    className="text-xl font-light leading-relaxed mt-4 mb-4 text-gray-700 text-center md:text-left"
                >
                    At Custom Lab, we take pride in being your go-to destination for custom stickers and t-shirts.
                    Our user-friendly online designing editor allows you to unleash your creativity and
                    bring your unique vision to life! <br/>
                    With just a few clicks, you can personalize your stickers and t-shirts,
                    making them truly one-of-a-kind.
                </p>
            </div>
            <div className="w-full lg:w-5/12 2xl:w-3/12 mr-auto ml-auto relative h-[300px] pt-10 lg:pt-0 order-1 md:order-2 mb-10 md:mb-0">
                <div
                    className="relative bg-white w-full shadow-lg h-full"
                >
                    <h1>VIDEO</h1>
                    <svg
                        preserveAspectRatio="none"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 583 95"
                        className="absolute left-0 w-full block bottom-0"
                    >
                        <polygon
                            points="-30,95 583,95 583,65"
                            className="text-mainColor fill-current"
                        ></polygon>
                    </svg>
                </div>
            </div>
        </div>
    )
}