export const TopBg = () => {

    return (
        <div
            className="relative lg:pt-[8rem] pt-[4rem] md:pt-5 lg:pb-[11rem] pb-[6rem] flex content-center items-center justify-center min-h-[57vh]"
        >
            <div
                className={`absolute top-0 w-full h-full bg-center bg-cover bg-hero-pattern bg-no-repeat bgHome`}
            >
            </div>
            <div className="container relative mx-auto">
                <div className="items-center flex flex-wrap">
                    <div className="w-full lg:px-4 px-2 ml-auto mr-auto text-center">
                        <div className="lg:pr-12">
                            <h1 className="font-semibold lg:text-7xl text-5xl text-mainColor text-HoltwoodOneSC tracking-[1rem] break-words">
                                CUSTOM LAB
                            </h1>
                            <p className="mt-4 md:text-5xl text-gray-300 text-ComingSoon py-7 text-[26px]">
                                CREATE YOUR OWN AWESOME CUSTOM PRINTS!
                            </p>
                            <p className={"text-white text-Allison md:text-6xl text-[50px]"}>
                                Making impressions that last
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div
                className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-[40px] bg-[#ebd303]"
            >
                <svg
                    className="absolute bottom-0 overflow-hidden"
                    xmlns="http://www.w3.org/2000/svg"
                    preserveAspectRatio="none"
                    version="1.1"
                    viewBox="0 0 2560 100"
                    x="0"
                    y="0"
                >
                    <polygon
                        className="text-[#d8d8d8] fill-current"
                        points="4500 30 2560 100 10 100"
                    ></polygon>
                </svg>
            </div>
            <div className="box-top"></div>
        </div>
    )
}