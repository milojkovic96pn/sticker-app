import React, {useRef, useState} from "react";
import Konva from "konva";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export const Sticker = () => {

    const stage = new Konva.Stage({
        container: 'sticker',
        width: 1000,
        height: 1000,
    })
    const layer = new Konva.Layer();
    stage.add(layer);

    const [photo,setPhoto] = useState<any>()
    Konva.Image.fromURL(photo, function (image) {
        layer.add(image);
        image.setAttrs({
            x: 80,
            y: 30,
            width:50,
            height:50,
            borderSize: 5,
            borderColor: 'red',
            position: "absolute",
        });

        image.filters([Border]);
        image.cache();
    });
    var canvas = document.createElement('canvas');
    var tempCanvas = document.createElement('canvas');

    const removeTransparency = (canvas:any) => {
        var ctx = canvas.getContext('2d');

        var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        var nPixels = imageData.data.length;
        for (var i = 3; i < nPixels; i += 4) {
            if (imageData.data[i] > 0) {
                imageData.data[i] = 255;
            }
        }
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.putImageData(imageData, 0, 0);
        return canvas;
    }
    const Border = (imageData:any) => {
        var nPixels = imageData.data.length;

        var size = "5";

        // - first set correct dimensions for canvases
        canvas.width = imageData.width;
        canvas.height = imageData.height;

        tempCanvas.width = imageData.width;
        tempCanvas.height = imageData.height;

        // - the draw original shape into temp canvas
        tempCanvas?.getContext('2d')?.putImageData(imageData, 0, 0);

        // - then we need to remove alpha chanel, because it will affect shadow (transparent shapes has smaller shadow)
        removeTransparency(tempCanvas);

        var ctx = canvas.getContext('2d');
        var color = "blue";

        // @ts-ignore
        ctx.save();
        // @ts-ignore
        ctx.shadowColor = color;
        // @ts-ignore
        ctx.shadowBlur = 5;
        // @ts-ignore
        ctx.drawImage(tempCanvas, 0, 0);
        // @ts-ignore
        ctx.restore();

        // - Then we will dive in into image data of [original image + shadow]
        // and remove transparency from shadow
        // @ts-ignore
        var tempImageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

        var SMOOTH_MIN_THRESHOLD = 3;
        var SMOOTH_MAX_THRESHOLD = 10;

        let val, hasValue;

        var offset = 3;

        for (var i = 3; i < nPixels; i += 4) {
            // skip opaque pixels
            if (imageData.data[i] === 255) {
                continue;
            }

            val = tempImageData.data[i];
            hasValue = val !== 0;
            if (!hasValue) {
                continue;
            }
            if (val > SMOOTH_MAX_THRESHOLD) {
                val = 255;
            } else if (val < SMOOTH_MIN_THRESHOLD) {
                val = 0;
            } else {
                val =
                    ((val - SMOOTH_MIN_THRESHOLD) /
                        (SMOOTH_MAX_THRESHOLD - SMOOTH_MIN_THRESHOLD)) *
                    255;
            }
            tempImageData.data[i] = val;
        }

        // draw resulted image (original + shadow without opacity) into canvas
        ctx?.putImageData(tempImageData, 0, 0);

        // then fill whole image with color (after that shadow is colored)
        ctx?.save();
        // @ts-ignore
        ctx.globalCompositeOperation = 'source-in';
        // @ts-ignore
        ctx.fillStyle = color;
        ctx?.fillRect(0, 0, canvas.width, canvas.height);
        ctx?.restore();

        // then we need to copy colored shadow into original imageData
        // @ts-ignore
        var newImageData = ctx.getImageData(0, 0, canvas.width, canvas.height);

        var indexesToProcess = [];
        for (var i = 3; i < nPixels; i += 4) {
            var hasTransparentOnTop =
                imageData.data[i - imageData.width * 4 * offset] === 0;
            var hasTransparentOnTopRight =
                imageData.data[i - (imageData.width * 4 + 4) * offset] === 0;
            var hasTransparentOnTopLeft =
                imageData.data[i - (imageData.width * 4 - 4) * offset] === 0;
            var hasTransparentOnRight = imageData.data[i + 4 * offset] === 0;
            var hasTransparentOnLeft = imageData.data[i - 4 * offset] === 0;
            var hasTransparentOnBottom =
                imageData.data[i + imageData.width * 4 * offset] === 0;
            var hasTransparentOnBottomRight =
                imageData.data[i + (imageData.width * 4 + 4) * offset] === 0;
            var hasTransparentOnBottomLeft =
                imageData.data[i + (imageData.width * 4 - 4) * offset] === 0;
            var hasTransparentAround =
                hasTransparentOnTop ||
                hasTransparentOnRight ||
                hasTransparentOnLeft ||
                hasTransparentOnBottom ||
                hasTransparentOnTopRight ||
                hasTransparentOnTopLeft ||
                hasTransparentOnBottomRight ||
                hasTransparentOnBottomLeft;

            // if pixel presented in original image - skip it
            // because we need to change only shadow area
            if (
                imageData.data[i] === 255 ||
                (imageData.data[i] && !hasTransparentAround)
            ) {
                continue;
            }
            if (!newImageData.data[i]) {
                // skip transparent pixels
                continue;
            }
            indexesToProcess.push(i);
        }

        for (var index = 0; index < indexesToProcess.length; index += 1) {
            var i = indexesToProcess[index];

            var alpha = imageData.data[i] / 255;

            if (alpha > 0 && alpha < 1) {
                var aa = 1 + 1;
            }
            imageData.data[i] = newImageData.data[i];
            imageData.data[i - 1] =
                newImageData.data[i - 1] * (1 - alpha) +
                imageData.data[i - 1] * alpha;
            imageData.data[i - 2] =
                newImageData.data[i - 2] * (1 - alpha) +
                imageData.data[i - 2] * alpha;
            imageData.data[i - 3] =
                newImageData.data[i - 3] * (1 - alpha) +
                imageData.data[i - 3] * alpha;

            if (newImageData.data[i] < 255 && alpha > 0) {
                var bb = 1 + 1;
            }
        }
    }
    const inputRef = useRef<HTMLInputElement>(null);
    const convertToBase64 = (file: any) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onload = () => {
                resolve(fileReader.result);
            };
            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };
    const handleFileUpload = async (e: any) => {
        const file = e.target.files[0];
        const base64 = await convertToBase64(file);
        e.preventDefault();
        setPhoto(base64 as string)
    };

    return (
        <div className={"p-[2rem]"}>
            <div className={"text-center text-[20px] w-full"}>
                <div
                    onClick={() => !photo && inputRef?.current?.click()}
                    className={"relative border border-[#484f5a] border-dashed  cursor-pointer m-auto rounded-[4px] flex justify-center items-center text-[#ddd] p-5"}
                >
                    {photo ?
                        <img className="object-cover w-[150px] h-[150px] rounded-[4px]" src={photo} alt="avatar" /> :
                        <div>
                            <FontAwesomeIcon
                                icon={["fas", "image"]}
                            />
                            <span className={"block"}>Click here</span>
                            <input
                                ref={inputRef}
                                type="file"
                                accept="image/*"
                                className={"hidden"}
                                onChange={handleFileUpload}
                            />
                        </div>
                    }
                </div>
            </div>
        </div>
    )
}