import {Modal} from "../../../components/Modals";
import React, {useEffect, useRef, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import clsx from "clsx";
import {useClickAway, useMedia} from "react-use";
import {HexColorPicker} from "react-colorful";
import styled from "styled-components";
import {
    makeMoveable,
    DraggableProps,
    ScalableProps,
    RotatableProps,
    Rotatable,
    Draggable,
    Scalable,
} from "react-moveable";
import MoveableHelper from "moveable-helper";
import mirror from "../../../assets/images/mirror.svg"
import mirrorVertical from "../../../assets/images/mirrorVertical.svg"
import {cartStore} from "../../../stores/cart";
import "../sticker";
import {stickerCanvas} from "../sticker";
import {useNavigate} from "react-router-dom";
import dieCut from "../../../assets/dieCut.png"
import Resizer from "react-image-file-resizer";
import {Simulate} from "react-dom/test-utils";
import {Toggle} from "../../../components/elements/toggle/Toggle";


const Wrapper = styled.div<{ color:string, border:any, shape:string, imgwidth:any,rounded:any,material:string}>`
  outline:${(props) => (props.material !== "Gold Chrome" && props.material !== "Chrome Silver") && `${props.border}px solid ${props.material !== "Transparent" ? props.color : "#ffffff26"}`};
  width:${(props) => props.shape === "circle" && `${props.imgwidth}px`};
  height:${(props) => props.shape === "circle" && `${props.imgwidth}px`};
  background:${(props) => props.material === "Chrome Silver" ? " linear-gradient(to right, #CCCCCC 0%, 38.21839094161987%, #FDFDFD 76.43678188323975%, 88.21839094161987%, #F2F2F2 100%); /* Gradient in RGBA */ linear-gradient(to right, rgba(204, 204, 204, 1) 0%, 38.21839094161987%, rgba(253, 253, 253, 1) 76.43678188323975%, 88.21839094161987%, rgba(242, 242, 242, 1) 100%);" : props.material === "Gold Chrome" ? "linear-gradient(to right, rgba(247, 239, 177, 1) 0%, 6.034482643008232%, rgba(235, 221, 153, 1) 12.068965286016464%, 56.03448264300823%, rgba(195, 159, 72, 1) 100%);" : props.material !== "Transparent" ? `${props.color}` : " "};
  border-radius: ${(props) => props.shape === "rounded" ? `${props.rounded}px` : props.shape === "circle" ? "50%" : "" };
  outline-style:${(props) => props.material === "Transparent" && `solid`};
  outline-color:${(props) => props.material === "Transparent" && `#bdbdbd`};
  outline-width:${(props) => props.material === "Transparent" && `thin`};

`;

const WrapperHeight = styled.div<{border:any}>`
  right:${(props) => props.border > 10 ? `-${props.border + 74}px` : "-72px"};

`;

const WrapperWidth = styled.div<{border:any}>`
  top:${(props) => props.border > 10 ? `-${props.border + 38}px` : "-38px"};

`;

const Rotate = styled.div<{rotate:any,scale:any}>`
  transform:${(props) => `rotate(${props.rotate}deg) scaleX(${props.scale})`};
`;

const ImgWrapper = styled.div<{ color:string}>`
  background:${(props) => `${props.color}`};
`;


const Moveable = makeMoveable<DraggableProps & ScalableProps & RotatableProps>([
    Draggable,
    Scalable,
    Rotatable,
]);
export const DesignSticker = () => {

    const navigate = useNavigate()

    const [showPreviewModal, setShowModalPreview] = useState<boolean>(true);
    const [showNotes, setShowNotes] = useState<boolean>(false);
    const [notes, setNotes] = useState<string>("");

    const [photo,setPhoto] = useState<any>("")
    const inputRef = useRef<HTMLInputElement>(null);
    const [width,setWidth] = useState<any>(0)
    const [height,setHeight] = useState<any>( 0)
    const [border,setBorder] = useState<any>( 5)

    const [quantity,setQuantity] = useState<number | string>(1)
    const [shape,setShape] = useState<string>("rounded")
    const [collapseShape, setCollapseShape] = useState(false);
    const [collapseMaterial, setCollapseMaterial] = useState(false);

    const refShape = useRef(null);
    const refMaterial = useRef(null);

    const [collapseColor, setCollapseColor] = useState(false);
    const refColor = useRef(null);

    const [color,setColor] = useState<any>("#fff")

    const [imgWidth,setImgWidth] = useState<number | string>( 0)
    const [imgHeight,setImgHeight] = useState<number | string>( 0)

    const [price,setPrice] = useState<number>(1)
    const targetRef = useRef<any>(null);
    const divRef = useRef<any>(null);

    const [helper] = React.useState(() => {
        return new MoveableHelper();
    });

    const [materialState,setMaterial] = useState("Premium")
    const [loading,setLoading] = useState<boolean>(false)

    const handleCollapseShape = () => {
        setCollapseShape(!collapseShape);
    };
    useClickAway(refShape, () => {
        if (collapseShape) {
            setTimeout(() => {
                handleCollapseShape();
            }, 100);
        }
    });

    const handleCollapseMaterial = () => {
        setCollapseMaterial(!collapseMaterial);
    };
    useClickAway(refMaterial, () => {
        if (collapseMaterial) {
            setTimeout(() => {
            }, 100);
        }
    });

    const handleCollapseColor = () => {
        setCollapseColor(!collapseColor);
    };
    useClickAway(refColor, () => {
        if (collapseColor) {
            setTimeout(() => {
                handleCollapseColor();
            }, 100);
        }
    });

    const isWide = useMedia("(min-width: 1400px)");
    const isLarge =  useMedia("(min-width: 1024px)");

    const [bgRemove,setBgRemove] = useState<any>(false)

    const [convertImage,setConvertImage] = useState<any>("")

    const handleChangeBg = async (e: any) => {
        setLoading(true)
        try {
            Resizer.imageFileResizer(
                e.target.files[0],
                isWide ? 600 : 350,
                isWide ? 600 : 350,
                `png`,
                100,
                0,
                (uri) => {
                    setConvertImage(uri);
                },
                "file",
                isWide ? 200 : 50,
                isWide ? 200 : 50
            );
        } catch (err) {
            console.log(err);
        }

    }

    useEffect(() => {
        if(convertImage) {
            setLoading(true)
            const apiKey = "TbMK5KqLRG3MTuauK6jfPv8N"
            const url = "https://api.remove.bg/v1.0/removebg"

            const formData = new FormData()
            formData.append('image_file',convertImage,"test")
            formData.append('size','auto')

            fetch(url, {
                method: 'POST',
                headers: {
                    'X-Api-Key': apiKey
                },
                body: formData
            }).then((res) =>res.blob()).then((blob) => {
                const reader = new FileReader()
                reader.onload = () => setPhoto(reader.result);setLoading(false)
                reader.readAsDataURL(blob)
            }).catch((error) => console.log(error))

            setShowModalPreview(false)

            let img = new Image()
            img.src = window.URL.createObjectURL(convertImage)
            img.onload = () => {
                setImgWidth((img.width*0.026/3).toFixed(1))
                setImgHeight((img.height*0.026/3).toFixed(1))
                setWidth((img.width*0.026/3).toFixed())
                setHeight((img.height*0.026/3).toFixed())
            }

        }
    },[convertImage])
    const handleFileUpload = async (e: any) => {
        setLoading(true)
        e.preventDefault();

        try {
            Resizer.imageFileResizer(
                e.target.files[0],
                isWide ? 600 : 350,
                isWide ? 600 : 350,
                `${e.target.files[0].type.split('/')[1].toUpperCase()}`,
                100,
                0,
                (uri) => {
                    setPhoto(uri);
                },
                "base64",
                isWide ? 200 : 50,
                isWide ? 200 : 50
            );
        } catch (err) {
            console.log(err);
        }

        // setPhoto(base64 as string)
        setShowModalPreview(false)

        let img = new Image()
        img.src = window.URL.createObjectURL(e.target.files[0])
        img.onload = () => {
            setImgWidth((img.width*0.026/3).toFixed(1))
            setImgHeight((img.height*0.026/3).toFixed(1))
            setWidth((img.width*0.026/3).toFixed(1))
            setHeight((img.height*0.026/3).toFixed(1))
            setLoading(false)
        }

    };

    useEffect(() => {
        if(shape === "circle") {
            setHeight(width)
        }
        if(shape === "rectangle" || shape === "rounded") {
            setHeight(imgHeight)
        }
        if(shape === "cut") {
            setHeight((Number(imgHeight)-1.5))
        }
    },[shape])

    const [rotate,setRotate] = useState(0)
    const topDown = () => {
        if(rotate === 0)
        {
            setRotate(180)
        } else {
            setRotate(0)
        }
    }

    const [scale,setScale] = useState(1)

    const leftRight = () => {
        if(scale === 1) {
            setScale(-1)
        } else {
            setScale(1)
        }
    }

    const [overflow, setOverflow] = useState(true)
    const [rounded,setrounded] = useState(25)

    const { addToCart } = cartStore()
    const addToCartFunction = () => {
        addToCart({quantity:quantity,price:Number(quantity) * 2,product:""})
        setPhoto("");
        setShowModalPreview(true);
        setNotes("")
    }

    const widthImgDemo = targetRef?.current?.width
    const heightImgDemo = targetRef?.current?.height

    const [widthImg,setWidthImg] = useState<any>(0)
    const [heightImg,setHeightImg] = useState<any>(0)

    useEffect(() => {
        if(widthImgDemo) {
            setWidthImg(widthImgDemo)
            setHeightImg(heightImgDemo)
        }
    },[widthImgDemo,heightImgDemo])

    useEffect(() => {
        if(shape === "cut") {
            stickerCanvas(color,border,photo,widthImg,heightImg)
        }
    },[shape])

    useEffect(() => {
        if(shape === "cut") {
            stickerCanvas(color,border,photo,widthImg,heightImg)
        }
    },[shape,color,border])

    const [divImg,setDivImg] = useState<any>(0)
    useEffect(() => {
        if(divRef?.current?.offsetWidth > 0) {
            setDivImg(divRef?.current?.offsetWidth)
        }
    },[divRef?.current?.offsetWidth])

    const setWidthFunction = (value:any) => {
        setWidth(value)
        setHeight((Number(height) - Number(width-value)).toFixed(1))
    }

    const setHeightFunction = (value:any) => {
        setHeight(Number.isInteger(Number(value)) ? Number(value) : Number(value).toFixed(1) )
        setWidth((Number(width) - Number(height-value)).toFixed(1))
    }

    const [collapseInfo, setCollapseInfo] = useState(false);

    return (
        <>
            <div
                className={clsx(
                    // `min-h-[calc(100vh_-_70px)] relative`,
                    `relative flex items-start justify-center`,
                    materialState === "Transparent" ? "transparent-material" : "bg-[#d8d8d8]",
                    isWide ? "h-[calc(100vh_-_159px)] p-[50px]" : "h-[calc(100vh_-_124px)] pl-[35px] pr-[20px]"
                )}
            >
                <div className={
                    clsx("absolute top-[33%] lg:left-5 bg-white lg:p-4 p-1 left-1 rounded-b-2xl rounded-t-2xl z-[98] text-center",
                        !isLarge && "border border-mainColor"
                    )}>
                    <div className={"pb-2 cursor-pointer"} onClick={() => setShowNotes(true)}>
                        <FontAwesomeIcon
                            icon={["fas", "comment"]}
                            className={"text-[#656565] lg:w-[34px] lg:h-[34px]"}
                        />
                    </div>
                    <div className={"pb-2 cursor-pointer"} onClick={leftRight}>
                        <img src={mirror} className={"lg:w-full w-[16px]"} />
                    </div>
                    <div className={"pb-2 cursor-pointer"} onClick={topDown}>
                        <img src={mirrorVertical} className={"lg:w-full w-[16px]"} />
                    </div>
                    <div className={"cursor-pointer"} onClick={() =>{ setPhoto("");setShowModalPreview(true); setNotes("");setShape("rounded")}}>
                        <FontAwesomeIcon
                            icon={["fas", "trash"]}
                            className={"text-[#656565] lg:w-[34px] lg:h-[34px]"}
                        />
                    </div>
                    {!isLarge ?
                        <div
                            onClick={() => setCollapseInfo(!collapseInfo)}
                            className={"absolute -bottom-10 left-0 flex justify-center items-center bg-mainColor rounded-[50%] w-[26px] h-[26px] z-[99]"}
                        >
                            <FontAwesomeIcon
                                icon={["fas", "question"]}
                            />
                        </div> : null
                    }
                </div>
                <div className={"flex justify-center items-center mt-[3rem] relative"}>
                    <div>
                        {shape !== "cut" && shape !== "circle" && isLarge && photo && !loading &&
                            <WrapperWidth
                                className={"text-center absolute right-[40%] text-[19px] font-bold text-[#222222] w-[100px]"}
                                border={border}
                            >
                                <span className={"text-center"}>{Number.isInteger(Number(width)) ? Number(width) : Number(width).toFixed(1)} cm</span>
                            </WrapperWidth>
                        }
                        {photo && !loading &&
                            <>
                                {shape !== "cut" ?
                                    <Wrapper
                                        className={clsx(
                                            `w-fit relative`,
                                            overflow ? "overflow-hidden" : "overflow-visible"
                                        )}
                                        color={color}
                                        border={border}
                                        imgwidth={divImg}
                                        shape={shape}
                                        rounded={rounded}
                                        material={materialState}
                                        onMouseUp={() => setOverflow(false)}
                                        ref={divRef}
                                    >
                                        <div
                                            className={clsx(
                                                `target`,
                                            )}
                                        >
                                            <Rotate
                                                rotate={rotate}
                                                scale={scale}
                                            >
                                                <img
                                                    ref={targetRef}
                                                    src={photo}
                                                    alt={"sticker-design"}
                                                    className={clsx(
                                                        `lg:max-h-[calc(100vh_-_400px)] p-[10px]`,
                                                        overflow ? "" : "opacity-[0.5]"
                                                    )}
                                                />
                                            </Rotate>
                                        </div>
                                        <Moveable
                                            target={!overflow ? targetRef : null}
                                            draggable={true}
                                            scalable={true}
                                            keepRatio={true}
                                            rotatable={true}
                                            onDragStart={helper.onDragStart}
                                            onDrag={helper.onDrag}
                                            onScaleStart={() => setOverflow(false)}
                                            onScaleEnd={() => setOverflow(true)}
                                            onDragEnd={() => setOverflow(true)}
                                            onScale={helper.onScale}
                                            onRotateStart={helper.onRotateStart}
                                            onRotate={helper.onRotate}
                                        />
                                    </Wrapper> :
                                    <div
                                        className={clsx(
                                            `pointer-events-none`,
                                        )}
                                    >
                                        <canvas id="design-canvas" className={"z-[9]"} />
                                    </div>
                                }
                            </>
                        }
                    </div>
                    {shape !== "cut" && shape !== "circle" && isLarge && photo && !loading &&
                        <WrapperHeight className={
                            clsx(
                                "text-[19px] absolute rotate-90 font-bold text-[#222222] w-[100px]",
                            )
                        }
                                       border={border}
                        >
                            <span>
                                {Number.isInteger(Number(height)) ? Number(height) : Number(height).toFixed(1)} cm
                            </span>
                        </WrapperHeight>
                    }
                </div>
                {loading &&
                    <div
                        className={"middle-absolute-front flex items-center justify-center relative overflow-hidden"}
                    >
                        <div className="lds-ring">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                }
                {isLarge ?
                    <div className={"absolute bottom-[1.5rem] right-8 flex"}>
                        <div className={"bg-white relative flex items-center rounded-l-lg"}>
                            <div className={"mx-5 font-bold text-lg"}>
                                {price}€
                            </div>
                            <div className={"price-arrow"}></div>
                        </div>
                        <div
                            className="no-highlight cursor-pointer bg-mainColor text-gray-800 active:bg-gray-100 text-[16px] font-bold uppercase px-4 py-2 rounded-r-lg shadow hover:shadow-md outline-none focus:outline-none"
                            onClick={addToCartFunction}
                        >
                            Add to cart
                        </div>
                    </div> : null
                }
                <Modal
                    show={showPreviewModal && !photo}
                    typeModal={"center"}
                    className={"relative xl:w-[30rem]"}
                    close={() => navigate('/')}
                >
                    <div className={"text-center text-[20px] w-full"}>
                        <div
                            className={"relative border border-[#484f5a] border-dashed  cursor-pointer m-auto rounded-[4px] flex justify-center items-center p-5"}
                        >
                            <div>
                                <FontAwesomeIcon
                                    icon={["fas", "image"]}
                                />
                                <span className={"block"}>Click here</span>
                                <input
                                    ref={inputRef}
                                    type="file"
                                    accept="image/*"
                                    className={"hidden"}
                                    onChange={bgRemove ? handleChangeBg : handleFileUpload}
                                />
                                <span
                                    className="block mt-3 cursor-pointer bg-mainColor text-gray-800 active:bg-gray-100 text-xs font-bold uppercase px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none lg:mr-1 lg:mb-0 ml-3 mb-3"
                                    onClick={() => !photo && inputRef?.current?.click()}
                                >
                                    Browse photo
                                </span>
                                <div className={"flex justify-center items-center gap-3 mt-5"}>
                                    Remove Background Image
                                    <Toggle
                                        rightComponent={
                                            <FontAwesomeIcon
                                                icon={["fas", "check"]}
                                                className="text-white"
                                            />
                                        }
                                        leftComponent={
                                            <FontAwesomeIcon
                                                icon={["fas", "x"]}
                                                className="text-white"
                                            />
                                        }
                                        value={bgRemove}
                                        onClick={() => setBgRemove(!bgRemove)}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal>
                <Modal show={showNotes} typeModal={"center"} close={() => setShowNotes(false)} >
                      <textarea
                          value={notes}
                          placeholder={"your message..."}
                          className={"w-full border border-[#22222] p-2 focus-none rounded-[5px] text-[#22222]"}
                          onChange={(e) => setNotes(e.target.value)}
                          rows={7}
                      />
                </Modal>
            </div>
            {isLarge ?
                <div className={
                    clsx("text-[#444] bottom-0 lg:h-[9.5vh] bg-white border-t-[1px] lg:border-mainColor w-full justify-center items-center font-bold lg:relative absolute lg:flex block top min-h-[100%] z-[999]",
                    )
                }>
                    <div className={"py-[13px] px-[6px] sticker-box text-center lg:border-r-[1px] lg:border-mainColor border-b lg:h-full"}>
                        <span className={"block text-[16px]"}>
                            width (cm)
                        </span>
                        <div className={"flex items-center justify-center"}>
                            <div>
                                <input
                                    type={"number"}
                                    placeholder={"add text..."}
                                    className={"focus-none rounded-[5px] text-center h-[30px] w-[80px] text-[20px]"}
                                    onChange={(e) => setWidthFunction(e.target.value)}
                                    value={Number.isInteger(Number(width)) ? Number(width) : Number(width).toFixed(1)}
                                    min={1}
                                    step={0.1}
                                />
                            </div>
                        </div>
                    </div>
                    <div className={"py-[13px] px-[6px] sticker-box text-center lg:border-r-[1px] lg:border-mainColor border-b lg:h-full"}>
                        <span className={"block text-[16px]"}>
                            height (cm)
                        </span>
                        <div className={"flex items-center justify-center"}>
                            <div>
                                <input
                                    type={"number"}
                                    placeholder={"add text..."}
                                    className={"focus-none rounded-[5px] text-center h-[30px] w-[80px] text-[20px]"}
                                    onChange={(e) => setHeightFunction(e.target.value)}
                                    value={Number.isInteger(Number(height)) ? Number(height) : Number(height).toFixed(1)}
                                    min={1}
                                    step={0.1}
                                />
                            </div>
                        </div>
                    </div>
                    <div className={"py-[13px] px-[6px] sticker-box text-center lg:border-r-[1px] lg:border-mainColor border-b lg:h-full"}>
                        <span className={"block text-[16px]"}>
                           quantity
                        </span>
                        <div className={"flex items-center justify-center"}>
                            <div>
                                <input
                                    type={"number"}
                                    placeholder={"add text..."}
                                    className={"focus-none rounded-[5px] text-center h-[30px] w-[80px] text-[20px]"}
                                    onChange={(e) => setQuantity(e.target.value)}
                                    value={quantity}
                                    min={1}
                                />
                            </div>
                        </div>
                    </div>
                    <div className={"py-[13px] px-[6px] sticker-box-border text-center lg:border-r-[1px] lg:border-mainColor border-b lg:h-full"}>
                        <span className={"block pb-2"}>
                            border
                        </span>
                        <span className={"block text-[16px] px-10"}>
                         <input
                             type="range"
                             min={0}
                             max={40}
                             step={1}
                             value={border}
                             onChange={event => {
                                 setBorder(event.target.valueAsNumber)
                             }}
                             className={"w-full"}
                         />
                       </span>
                    </div>
                    {shape === "rounded" &&
                        <div className={"py-[13px] px-[6px] sticker-box-border text-center lg:border-r-[1px] lg:border-mainColor border-b lg:h-full"}>
                            <span className={"block pb-2"}>
                                rounded
                            </span>
                            <span className={"block text-[16px] px-10"}>
                         <input
                             type="range"
                             min={0}
                             max={300}
                             step={1}
                             value={rounded}
                             onChange={event => {
                                 setrounded(event.target.valueAsNumber)
                             }}
                             className={"w-full"}
                         />
                    </span>
                        </div>
                    }
                    <div
                        className={"py-[13px] px-[6px] sticker-box text-center lg:border-r-[1px] lg:border-mainColor relative cursor-pointer border-b lg:h-full"}
                        ref={refColor}
                    >
                        <div
                            onClick={() => handleCollapseColor()}
                        >
                      <span className={"block"} >
                        color
                      </span>
                            <span className={"block"}>
                          <FontAwesomeIcon
                              icon={["fas", "paint-brush"]}
                              className={"mt-2 text-[22px]"}
                          />
                      </span>
                        </div>
                        {collapseColor &&
                            <div className={"absolute -top-[14rem] right-[1rem] rounded-[10px] bg-white"}>
                                <HexColorPicker color={color} onChange={setColor} />
                            </div>
                        }
                    </div>
                    <div
                        className={"py-[13px] px-[6px] sticker-box text-center border-r-[1px] lg:border-mainColor relative cursor-pointer border-b lg:h-full"}
                        ref={refShape}
                        onClick={() => handleCollapseShape()}
                    >
                        <span className={"block"}>
                            shape
                        </span>
                        <span className={"block"}>
                       <FontAwesomeIcon
                           icon={["fas", "scissors"]}
                           className={"mt-2 text-[22px]"}
                       />
                       </span>
                        {collapseShape && (
                            <div className={"absolute -top-[25rem] right-0 w-full bg-white border lg:border-[#ccc]"}>
                                <div
                                    className={clsx(
                                        `p-3 hover:bg-[#e0e2e18a]`,
                                        shape === "cut" ? "bg-mainColor" : "even:bg-[#f8f9f8]"
                                    )}
                                    onClick={() => setShape("cut")}
                                >
                                   <span>
                                       DIE CUT
                                   </span>
                                    <img src={dieCut} alt={"die-cut"} className={"mt-1 w-[50px] h-[50px] m-auto"} />
                                </div>
                                <div
                                    className={clsx(
                                        `p-3 hover:bg-[#e0e2e18a]`,
                                        shape === "circle" ? "bg-mainColor" : "even:bg-[#f8f9f8]"
                                    )}
                                    onClick={() => setShape("circle")}
                                >
                               <span>
                                   CIRCLE
                               </span>
                                    <span className={"mt-1 w-[50px] h-[50px] rounded-[25px] border border-[#e0e2e1] block m-auto"}>
                               </span>
                                </div>
                                <div
                                    className={clsx(
                                        `p-3 hover:bg-[#e0e2e18a]`,
                                        shape === "rectangle" ? "bg-mainColor" : "even:bg-[#f8f9f8]"
                                    )}
                                    onClick={() => setShape("rectangle")}
                                >
                               <span>
                                   RECTANGLE
                               </span>
                                    <span className={"mt-1 w-[70px] h-[40px] border border-[#e0e2e1] block m-auto"}>
                               </span>
                                </div>
                                <div
                                    className={clsx(
                                        `p-3 hover:bg-[#e0e2e18a]`,
                                        shape === "rounded" ? "bg-mainColor" : "even:bg-[#f8f9f8]"
                                    )}
                                    onClick={() => setShape("rounded")}
                                >
                               <span>
                                   ROUNDED
                               </span>
                                    <span className={"mt-1 w-[70px] h-[40px] rounded-[10px] border border-[#e0e2e1] block m-auto"}>
                               </span>
                                </div>
                            </div>
                        )}
                    </div>
                </div> : null
            }
            {!isLarge && !collapseInfo ?
                <div className={"flex fixed bottom-0 w-full bg-[#272c33] text-[#b7b7b7]"}>
                    <div
                        className={clsx(
                            `py-[1rem] break-words text-[13px] text-center cursor-pointer lg:min-w-full hover:text-[#546e7a] lg:w-full w-[25%]`,
                            shape === "cut" ? "bg-[#3f4652] text-[#eee] lg:border-l-2 border-b-2 lg:border-b-0 border-mainColor" : ""
                        )}
                        onClick={() => setShape("cut")}
                    >
                         <span className={"text-[13px]"}>
                           DIE CUT
                       </span>
                        <img src={dieCut} alt={"die-cut"} className={"mt-1 w-[30px] h-[30px] m-auto"} />
                    </div>
                    <div
                        className={clsx(
                            `py-[1rem] break-words text-[13px] text-center cursor-pointer lg:min-w-full hover:text-[#546e7a] lg:w-full w-[25%]`,
                            shape === "circle" ? "bg-[#3f4652] text-[#eee] lg:border-l-2 border-b-2 lg:border-b-0 border-mainColor" : ""
                        )}
                        onClick={() => setShape("circle")}
                    >
                         <span className={"text-[13px]"}>
                           CIRCLE
                       </span>
                        <span className={"mt-1 w-[30px] h-[30px] rounded-[25px] border border-[#e0e2e1] block m-auto"}></span>
                    </div>
                    <div
                        className={clsx(
                            `py-[1rem] break-words text-[13px] text-center cursor-pointer lg:min-w-full hover:text-[#546e7a] lg:w-full w-[25%]`,
                            shape === "rectangle" ? "bg-[#3f4652] text-[#eee] lg:border-l-2 border-b-2 lg:border-b-0 border-mainColor" : ""
                        )}
                        onClick={() => setShape("rectangle")}
                    >
                        <span className={"text-[13px]"}>
                           RECTANGLE
                       </span>
                        <span className={"mt-1 w-[50px] h-[30px] border border-[#e0e2e1] block m-auto"}></span>
                    </div>
                    <div
                        className={clsx(
                            `py-[1rem] break-words text-[13px] text-center cursor-pointer lg:min-w-full hover:text-[#546e7a] lg:w-full w-[25%]`,
                            shape === "rounded" ? "bg-[#3f4652] text-[#eee] lg:border-l-2 border-b-2 lg:border-b-0 border-mainColor" : ""
                        )}
                        onClick={() => setShape("rounded")}
                    >
                       <span className={"text-[13px]"}>
                           ROUNDED
                       </span>
                        <span className={"mt-1 w-[50px] h-[30px] rounded-[10px] border border-[#e0e2e1] block m-auto"}>
                        </span>
                    </div>
                </div> : null
            }
            {!isLarge && collapseInfo ?
                <div className={"fixed bottom-0 w-full"}>
                    <div className={"flex relative bottom-0"}>
                        <div className={"absolute top-[-53px] bg-white flex justify-center items-center w-full border-b"}>
                            <div className={"absolute top-[-61px] text-center flex justify-center items-center bg-[#272c33] text-[#b7b7b7]"}>
                                <div className={"text-center py-1 px-2 border-r w-[60px]"}
                                     onClick={() => setCollapseInfo(false)}
                                >
                            <span className={"block"}>
                            shape
                            </span>
                                    <span className={"block"}>
                           <FontAwesomeIcon
                               icon={["fas", "scissors"]}
                               className={"mt-2 text-[16px]"}
                           />
                           </span>
                                </div>
                                <div className={"text-center py-1 px-2 w-[60px]"}
                                     onClick={() => handleCollapseColor()}
                                >
                            <span className={"block"}>
                            color
                            </span>
                                    <span className={"block"}>
                           <FontAwesomeIcon
                               icon={["fas", "paint-brush"]}
                               className={"mt-2 text-[16px]"}
                           />
                           </span>
                                    {collapseColor &&
                                        <div className={"absolute -top-[14rem] right-[1rem] rounded-[10px] bg-white"}>
                                            <HexColorPicker color={color} onChange={setColor} />
                                        </div>
                                    }
                                </div>
                            </div>
                            <div className={
                                clsx("py-[3px] px-[6px] bg-[#272c33] text-[#b7b7b7]",
                                    shape === "rounded" ? "w-[50%]" : "w-full"
                                )
                            }>
                            <span className={"text-left text-[9px]"}>
                                border
                            </span>
                                <span className={"block text-[16px]"}>
                             <input
                                 type="range"
                                 min={0}
                                 max={40}
                                 step={1}
                                 value={border}
                                 onChange={event => {
                                     setBorder(event.target.valueAsNumber)
                                 }}
                                 className={"w-full"}
                             />
                           </span>
                            </div>
                            {shape === "rounded" &&
                                <div className={"py-[3px] px-[6px] w-[50%] bg-[#272c33] text-[#b7b7b7]"}>
                            <span className={"text-left text-[9px]"}>
                                rounded
                            </span>
                                    <span className={"block text-[16px]"}>
                         <input
                             type="range"
                             min={0}
                             max={300}
                             step={1}
                             value={rounded}
                             onChange={event => {
                                 setrounded(event.target.valueAsNumber)
                             }}
                             className={"w-full"}
                         />
                                 </span>
                                </div>
                            }
                        </div>
                        <div className={"py-[1rem] break-words text-[13px] text-center cursor-pointer lg:min-w-full hover:text-[#546e7a] lg:w-full w-[22%] bg-[#272c33] text-[#b7b7b7]"}>
                        <span className={"block text-[16px] text-center"}>
                            width cm
                        </span>
                            <div className={"flex items-center justify-center"}>
                                <div>
                                    <input
                                        type={"number"}
                                        placeholder={"add text..."}
                                        className={"focus-none rounded-[5px] text-center h-[30px] w-[80px] text-[20px]"}
                                        onChange={(e) => setWidthFunction(e.target.value)}
                                        value={Number.isInteger(Number(width)) ? Number(width) : Number(width).toFixed(1)}
                                        min={1}
                                        step={0.1}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className={"py-[1rem] break-words text-[13px] text-center cursor-pointer lg:min-w-full hover:text-[#546e7a] lg:w-full w-[22%] bg-[#272c33] text-[#b7b7b7]"}>
                        <span className={"block text-[16px] text-center"}>
                            height cm
                        </span>
                            <div className={"flex items-center justify-center"}>
                                <div>
                                    <input
                                        type={"number"}
                                        placeholder={"add text..."}
                                        className={"focus-none rounded-[5px] text-center h-[30px] w-[80px] text-[20px]"}
                                        onChange={(e) => setHeightFunction(e.target.value)}
                                        value={Number.isInteger(Number(height)) ? Number(height) : Number(height).toFixed(1)}
                                        min={1}
                                        step={0.1}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className={"py-[1rem] break-words text-[13px] text-center cursor-pointer lg:min-w-full hover:text-[#546e7a] lg:w-full w-[22%] bg-[#272c33] text-[#b7b7b7]"}>
                        <span className={"block text-[16px] text-center"}>
                           quantity
                        </span>
                            <div className={"flex items-center justify-center"}>
                                <div>
                                    <input
                                        type={"number"}
                                        placeholder={"add text..."}
                                        className={"focus-none rounded-[5px] text-center h-[30px] w-[80px] text-[20px]"}
                                        onChange={(e) => setQuantity(e.target.value)}
                                        value={quantity}
                                        min={1}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className={"border-l py-[1rem] break-words text-[13px] text-center cursor-pointer lg:min-w-full hover:text-[#546e7a] lg:w-full w-[22%] bg-[#272c33] text-[#b7b7b7] font-bold text-lg flex justify-center items-center"}>
                            {price}€
                        </div>
                        <div
                            className="no-highlight cursor-pointer bg-mainColor font-bold w-[17%] flex justify-center items-center gap-1 text-white"
                            onClick={addToCartFunction}
                        >
                            add
                            <FontAwesomeIcon
                                icon={["fas", "cart-shopping"]}
                            />
                        </div>
                    </div>
                </div>
                : null
            }
        </>
    )
}