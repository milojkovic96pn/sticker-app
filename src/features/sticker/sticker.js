import pineaple from "../../assets/pineapple-removebg-preview.png";
import {fabric} from "fabric";

export const stickerCanvas = (color,border,photo,width = 700,height = 700) => {

    var img=new Image();
    img.onload=start;
    img.src=photo;

    var canvas = new fabric.Canvas('design-canvas', {
        width: width + 100,
        height: height + 100,
    });

    function start(){
        var stickerCanvas=stickerEffect(img,border);
        var imgInstance = new fabric.Image(stickerCanvas,{
            selectable:false,
        })
        canvas.add(imgInstance);
    }

    function stickerEffect(img,grow){
        var canvas1=document.createElement("canvas");
        var ctx1=canvas1.getContext("2d");
        var canvas2=document.createElement("canvas");
        var ctx2=canvas2.getContext("2d");
        canvas1.width=canvas2.width=img.width+grow*2;
        canvas1.height=canvas2.height=img.height+grow*2;
        ctx1.drawImage(img,grow,grow);
        ctx2.shadowColor=color;
        ctx2.shadowBlur=2;
        for(var i=0;i<grow;i++){
            ctx2.drawImage(canvas1,0,0);
            ctx1.drawImage(canvas2,0,0);
        }
        ctx2.shadowColor='rgba(0,0,0,0)';
        ctx2.drawImage(img,grow,grow);
        return(canvas2);
    }
}