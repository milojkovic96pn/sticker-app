import {cartStore} from "../../../stores/cart";
import {formatPrice} from "../../../helpers/formatPrice";
import {useState} from "react";
import frontShirt from "../../../assets/images/tshirt/front-shirt.png"
import frontShirtWoman from "../../../assets/images/tshirt/front-shirt-woman.png"
import clsx from "clsx";

export const Cart = () => {

    const { addToCart,cart } = cartStore()
    let price = cart?.reduce(function (acc: any, obj: any) {
        return acc + obj.price;
    }, 0)

    const [shipping, setShipping] = useState(3)

    return (
       <>
           <div className="flex flex-col items-center border-b bg-white py-4 sm:flex-row sm:px-10 lg:px-20 xl:px-32">
               <div className="mt-4 py-2 text-xs sm:mt-0 sm:ml-auto sm:text-base">
                   <div className="relative">
                       <ul className="relative flex w-full items-center justify-between space-x-2 sm:space-x-4">
                           <li className="flex items-center space-x-3 text-left sm:space-x-4">
                               <a className="flex h-6 w-6 items-center justify-center rounded-full bg-emerald-200 text-xs font-semibold text-emerald-700"
                                  href="#"
                               >
                                   <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none"
                                        viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                       <path stroke-linecap="round" stroke-linejoin="round" d="M5 13l4 4L19 7"/>
                                   </svg
                                   >
                               </a>
                               <span className="font-semibold text-gray-900">Design</span>
                           </li>
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-gray-400" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                               <path stroke-linecap="round" stroke-linejoin="round" d="M9 5l7 7-7 7"/>
                           </svg>
                           <li className="flex items-center space-x-3 text-left sm:space-x-4">
                               <a className="flex h-6 w-6 items-center justify-center rounded-full bg-gray-600 text-xs font-semibold text-white ring ring-gray-600 ring-offset-2"
                                  href="#">2</a>
                               <span className="font-semibold text-gray-900">Shipping</span>
                           </li>
                           <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-gray-400" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                               <path stroke-linecap="round" stroke-linejoin="round" d="M9 5l7 7-7 7"/>
                           </svg>
                           <li className="flex items-center space-x-3 text-left sm:space-x-4">
                               <a className="flex h-6 w-6 items-center justify-center rounded-full bg-gray-400 text-xs font-semibold text-white"
                                  href="#">3</a>
                               <span className="font-semibold text-gray-500">Payment</span>
                           </li>
                       </ul>
                   </div>
               </div>
           </div>
           <div className="grid sm:px-10 lg:grid-cols-2 lg:px-20 xl:px-32">
               <div className="px-4 pt-8">
                   <p className="text-xl font-medium">Order Summary</p>
                   <p className="text-gray-400">Check your items. And select a suitable shipping method.</p>
                   <div className="mt-8 space-y-3 rounded-lg border bg-white px-2 py-4 sm:px-6">
                       {cart.map((item:any,index:number) =>
                           <div className="flex flex-col rounded-lg bg-white sm:flex-row" key={index}>
                               <div className={"w-[96px] h-[96px]"}>
                                   <img
                                       src={item.productType === "Man" ? frontShirt : frontShirtWoman}
                                       alt="cart-preview"
                                       className={clsx(
                                           `m-2 h-24 w-28 rounded-md border object-cover object-center bg-${item.color}`,
                                           item.productType === "Man" && "w-[90px]",
                                       )}
                                   />
                               </div>
                               <div className="flex w-full flex-col px-4 py-4">
                                   <span className="font-semibold">
                                       {item.productType} T-Shirt x {item.quantity} {item.notes && `(${item.notes})`}
                                   </span>
                                   <span className="float-right text-gray-400">Size {item.size}</span>
                                   <p className="text-lg font-bold">
                                       {formatPrice(item.price, "USD")}
                                   </p>
                               </div>
                           </div>
                       )}
                   </div>

                   <p className="mt-8 text-lg font-medium">Shipping Methods</p>
                   <form className="mt-5 grid gap-6">
                       <div className="relative">
                           <input className="peer hidden" id="radio_1" type="radio" name="radio" checked/>
                           <span
                               className="peer-checked:border-gray-700 absolute right-4 top-1/2 box-content block h-3 w-3 -translate-y-1/2 rounded-full border-8 border-gray-300 bg-white"></span>
                           <label
                               className="peer-checked:border-2 peer-checked:border-gray-700 peer-checked:bg-gray-50 flex cursor-pointer select-none rounded-lg border border-gray-300 p-4"
                               htmlFor="radio_1">
                               <img className="w-14 object-contain" src="/images/naorrAeygcJzX0SyNI4Y0.png" alt=""/>
                               <div className="ml-5">
                                   <span className="mt-2 font-semibold">Fedex Delivery</span>
                                   <p className="text-slate-500 text-sm leading-6">Delivery: 2-4 Days</p>
                               </div>
                           </label>
                       </div>
                       <div className="relative">
                           <input className="peer hidden" id="radio_2" type="radio" name="radio" checked/>
                           <span
                               className="peer-checked:border-gray-700 absolute right-4 top-1/2 box-content block h-3 w-3 -translate-y-1/2 rounded-full border-8 border-gray-300 bg-white"></span>
                           <label
                               className="peer-checked:border-2 peer-checked:border-gray-700 peer-checked:bg-gray-50 flex cursor-pointer select-none rounded-lg border border-gray-300 p-4"
                               htmlFor="radio_2">
                               <img className="w-14 object-contain" src="/images/oG8xsl3xsOkwkMsrLGKM4.png" alt=""/>
                               <div className="ml-5">
                                   <span className="mt-2 font-semibold">Fedex Delivery</span>
                                   <p className="text-slate-500 text-sm leading-6">Delivery: 2-4 Days</p>
                               </div>
                           </label>
                       </div>
                   </form>
               </div>
               <div className="mt-10 bg-gray-50 px-4 pt-8 lg:mt-0">
                   <p className="text-xl font-medium">Payment Details</p>
                   <p className="text-gray-400">Complete your order by providing your payment details.</p>
                   <div className="">
                       <label htmlFor="email" className="mt-4 mb-2 block text-sm font-medium">Full Name</label>
                       <div className="relative">
                           <input type="text" id="email" name="email"
                                  className="w-full rounded-md border border-gray-200 px-4 py-3 pl-4 text-sm shadow-sm outline-none focus:z-10 focus:border-blue-500 focus:ring-blue-500"
                                  placeholder="You Full Name"/>
                       </div>
                       <label htmlFor="card-holder" className="mt-4 mb-2 block text-sm font-medium">Contact</label>
                       <div className="flex">
                           <div className="relative w-6/12">
                               <input type="text" id="card-no" name="card-no"
                                      className="w-full rounded-md border border-gray-200 px-2 py-3 pl-4 text-sm shadow-sm outline-none focus:z-10 focus:border-blue-500 focus:ring-blue-500"
                                      placeholder="your.email@gmail.com"/>
                           </div>
                           <div className="relative w-6/12 ml-2">
                               <input type="text" id="card-no" name="card-no"
                                      className="w-full rounded-md border border-gray-200 px-2 py-3 pl-4 text-sm shadow-sm outline-none focus:z-10 focus:border-blue-500 focus:ring-blue-500"
                                      placeholder="Phone"/>
                           </div>
                       </div>
                       <label htmlFor="card-no" className="mt-4 mb-2 block text-sm font-medium">Address</label>
                       <div className="flex">
                           <div className="relative w-6/12">
                               <input type="text" id="card-no" name="card-no"
                                      className="w-full rounded-md border border-gray-200 px-2 py-3 pl-4 text-sm shadow-sm outline-none focus:z-10 focus:border-blue-500 focus:ring-blue-500"
                                      placeholder="Country"/>
                           </div>
                           <div className="relative w-6/12 ml-2">
                               <input type="text" id="card-no" name="card-no"
                                      className="w-full rounded-md border border-gray-200 px-2 py-3 pl-4 text-sm shadow-sm outline-none focus:z-10 focus:border-blue-500 focus:ring-blue-500"
                                      placeholder="State"/>
                           </div>
                       </div>
                       <div className="flex mt-2">
                           <div className="relative w-6/12">
                               <input type="text" id="card-no" name="card-no"
                                      className="w-full rounded-md border border-gray-200 px-2 py-3 pl-4 text-sm shadow-sm outline-none focus:z-10 focus:border-blue-500 focus:ring-blue-500"
                                      placeholder="Adress"/>
                           </div>
                           <div className="relative w-6/12 ml-2">
                               <input type="text" id="card-no" name="card-no"
                                      className="w-full rounded-md border border-gray-200 px-2 py-3 pl-4 text-sm shadow-sm outline-none focus:z-10 focus:border-blue-500 focus:ring-blue-500"
                                      placeholder="Zip"/>
                           </div>
                       </div>
                       <label htmlFor="card-no" className="mt-4 mb-2 block text-sm font-medium">Notes</label>
                       <textarea
                              className="w-full rounded-md border border-gray-200 px-2 py-3 pl-4 text-sm shadow-sm outline-none focus:z-10 focus:border-blue-500 focus:ring-blue-500"
                              rows={7}
                       />

                       <div className="mt-6 border-t border-b py-2">
                           <div className="flex items-center justify-between">
                               <p className="text-sm font-medium text-gray-900">
                                  Subtotal
                               </p>
                               <p className="font-semibold text-gray-900">
                                   {formatPrice(price, "USD")}
                               </p>
                           </div>
                           <div className="flex items-center justify-between">
                               <p className="text-sm font-medium text-gray-900">Shipping</p>
                               <p className="font-semibold text-gray-900">
                                   {formatPrice(shipping, "USD")}
                               </p>
                           </div>
                       </div>
                       <div className="mt-6 flex items-center justify-between">
                           <p className="text-sm font-medium text-gray-900">Total</p>
                           <p className="text-2xl font-semibold text-gray-900">
                               {formatPrice(price + shipping, "USD")}
                           </p>
                       </div>
                   </div>
                   <button className="mt-4 mb-8 w-full rounded-md bg-gray-900 px-6 py-3 font-medium text-white">Place
                       Order
                   </button>
               </div>
           </div>

       </>
    )
}