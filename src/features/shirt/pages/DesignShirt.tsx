import frontShirt from "../../../assets/images/tshirt/front-shirt.png"
import backShirt from "../../../assets/images/tshirt/back-shirt.png"
import frontShirtWoman from "../../../assets/images/tshirt/front-shirt-woman.png"
import frontShirtWomanDark from "../../../assets/images/tshirt/front-shirt-woman-dark.png"
import backShirtWoman from "../../../assets/images/tshirt/back-shirt-woman.png"
import backShirtWomanDark from "../../../assets/images/tshirt/back-shirt-woman-dark.png"
import rightSleeveMen from "../../../assets/images/tshirt/men-right-sleeve.png"
import leftSleeveMen from "../../../assets/images/tshirt/men-left-sleeve.png"
import rightSleeveWomen from "../../../assets/images/tshirt/women-right-sleeve.png"
import leftSleeveWomen from "../../../assets/images/tshirt/women-left-sleeve.png"
import React, {useEffect, useRef, useState} from "react";
import clsx from "clsx";
import {PreviewShirt} from "../components/front/PreviewShirt";
import {ConfiguratorComponent} from "../components/ConfiguratorComponent";
import {PreviewShirtBack} from "../components/back/PreviewShirtBack";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {cartStore} from "../../../stores/cart";
import {PreviewRightSleeve} from "../components/rightSleeve/PreviewRightSleeve";
import {PreviewLeftSleeve} from "../components/leftSleeve/PreviewLeftSleeve";
import {fabric} from "fabric";
import deleteIcon from "../../../assets/delete-fabric.svg"
import {Modal} from "../../../components/Modals";
import {useNavigate} from "react-router-dom";
import manShirt from "../../../assets/manShirt.jpg";
import womanShirt from "../../../assets/womanShirt.jpg";
import {useMedia} from "react-use";

export const DesignShirt = () => {

    const { addToCart,cart } = cartStore()

    const [loader,setLoader] = useState(true)
    const [showPreviewModal, setShowModalPreview] = useState<boolean>(true);
    const [shirtPosition,setShirtPosition ] = useState<string>("")
    const [shirtColor,setShirtColor ] = useState<string>("whiteShirt")
    const [notes,setNotes] = useState<string>("")
    const [tab, setTab]=useState<string>("info")
    const [size,setSize ] = useState<string>("")
    const [quantity,setQuantity ] = useState<number>(1)
    const [product,setProduct ] = useState<string | null>(null)

    const [textFront, setTextFront] = useState<any>(null)
    const [textBack, setTextBack] = useState<any>(null)
    const [photoFront,setPhotoFront] = useState<any>(null)
    const [photoBack,setPhotoBack] = useState<any>(null)
    const [textLeftSleeve, setTextLeftSleeve] = useState<any>(null)
    const [textRightSleeve, setTextRightSleeve] = useState<any>(null)
    const [rightSleevePhoto,setRightSleevePhoto] = useState<any>(null)
    const [leftSleevePhoto,setLeftSleevePhoto] = useState<any>(null)

    const inputRef = useRef<HTMLInputElement>(null);
    const navigate = useNavigate()
    const isWide = useMedia("(min-width: 1024px)");

    const [loaderProduct, setLoaderProduct] = useState<boolean>(false)

    const canvasFront = useRef<any>(null);
    const canvasBack = useRef<any>(null);
    const canvasRight = useRef<any>(null);
    const canvasLeft = useRef<any>(null);

    useEffect(() => {
        document.title = `Custom Lab | Design T-Shirt`;
    }, []);

    const addToCartFunction = () => {
        addToCart({quantity:quantity,price:quantity * 2, productType: product,size:size,color:shirtColor,notes:notes,
            product:[
                {front: canvasFront.current.toDataURL({format: "png"})},
                {back: canvasBack.current.toDataURL({format: "png"})},
                {right: canvasRight.current.toDataURL({format: "png"})},
                {left: canvasLeft.current.toDataURL({format: "png"})}
            ]})
        setShirtColor("whiteShirt")
        setNotes("")
        setQuantity(1)
        setTextFront(null)
        setTextBack(null)
        setTextRightSleeve(null)
        setTextLeftSleeve(null)
        setPhotoFront(null)
        setPhotoBack(null)
        setProduct(null)
        setShirtPosition("")
        setShowModalPreview(true)
        canvasFront.current.clear()
        canvasBack.current.clear()
        canvasRight.current.clear()
        canvasLeft.current.clear()
    }
    const changePositionShirt = (type:string) => {
        setShirtPosition(type)
    }

    const changeColorShirt = (type:string) => {
        setShirtColor(type)
    }

    const chooseSize = (size:string) => {
        setSize(size)
    }
    const changeTab = (tabName:string) => {
        setTab(tabName)
    }

    const convertToBase64 = (file: any) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);
            fileReader.onload = () => {
                resolve(fileReader.result);
            };
            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };
    const handleFileUpload = async (e: any) => {
        const file = e.target.files[0];
        const base64 = await convertToBase64(file);
        e.preventDefault();
        if(shirtPosition === "front") {
            setPhotoFront({path:base64 as string})
        }
        if (shirtPosition === "back") {
            setPhotoBack({path:base64 as string})
        }
        if (shirtPosition === "right") {
            setRightSleevePhoto({path:base64 as string})
        }
        if (shirtPosition === "left") {
            setLeftSleevePhoto({path:base64 as string})
        }
    };

    const activeColor = (color:string, type:string) => {
        if((type === "shirt" && shirtColor === color) || (type === "text" && (shirtPosition === "front" ? textFront.textColor : textBack.textColor) === color))
         return  <div className="middle-absolute">
                    <FontAwesomeIcon
                        icon={["fas", "check"]}
                        className={"text-[#dddd]"}
                    />
                </div>
    }

    const [undoRedo, setUndoRedo] = useState<any>([])

    var img = new Image();
    img.src = deleteIcon;

    const renderIcon = (ctx:any, left:any, top:any, styleOverride:any, fabricObject:any) => {
        var size = 20;
        ctx.save();
        ctx.translate(left, top);
        ctx.rotate(fabric.util.degreesToRadians(fabricObject.angle));
        ctx.drawImage(img, -size/2, -size/2, size, size);
        ctx.restore();
    }

    useEffect(() => {
        fabric.Object.prototype.controls.deleteControl  = new fabric.Control({
            x: 0.5,
            y: -0.5,
            offsetY: -20,
            offsetX: 16,
            cursorStyle: 'pointer',
            render: renderIcon,
            // @ts-ignore
            mouseUpHandler: deleteObject,
        })
    },[])

    const deleteObject = (_:any, transform:any) => {
        var target = transform.target;
        var canvas = target.canvas;
        canvas.remove(target)

        let allObjects = [...undoRedo]
        allObjects.push(target)
        setUndoRedo(allObjects)

        canvas.requestRenderAll();
    }

    return (
        <div className={clsx("lg:flex flex-col lg:flex-row justify-start lg:max-h-[calc(100vh_-_68px)]",
            !isWide && tab !== "preview" && "bg-[#3f4652]",
            showPreviewModal && !product ? "lg:min-h-[calc(100vh_-_68px)]" : "lg:max-h-[calc(100vh_-_68px)]"
            )}>
            <ConfiguratorComponent
                tab={tab}
                changeTab={changeTab}
                size={size}
                changeColorShirt={changeColorShirt}
                inputRef={inputRef}
                activeColor={activeColor}
                chooseSize={chooseSize}
                setQuantity={setQuantity}
                quantity={quantity}
                imageUpload={shirtPosition === "front" ? photoFront : shirtPosition === "back" ? photoBack : shirtPosition === "right" ? rightSleevePhoto : leftSleevePhoto}
                notes={notes}
                setNotes={setNotes}
                handleFileUpload={handleFileUpload}
                text={shirtPosition === "front" ? textFront : shirtPosition === "back" ? textBack : shirtPosition === "right" ? textRightSleeve : textLeftSleeve}
                setText={shirtPosition === "front" ? setTextFront : shirtPosition === "back" ? setTextBack : shirtPosition === "right" ? setTextRightSleeve : setTextLeftSleeve}
                productType={product}
                setProduct={setProduct}
                setLoaderProduct={setLoaderProduct}
            />
            <div
                className={clsx(
                    `relative lg:w-[80%] flex justify-center  items-center lg:overflow-x-auto no-highlight min-h-[100vh] lg:min-h-[calc(100vh_-_68px)] order-1 lg:order-2 lg:mb-0 mb-[76px]`,
                    (!isWide && tab === "preview") || isWide && product !== null ? "visible" : "invisible !absolute top-0"
                )}
            >
                {!loaderProduct ?
                    <>
                        <PreviewShirt
                            shirtColor={shirtColor}
                            image={product ? (product === "Man" ? frontShirt : shirtColor === "white" ? frontShirtWoman : frontShirtWomanDark) : null}
                            textFront={textFront}
                            shirtPosition={shirtPosition}
                            imageUpload={photoFront}
                            productType={product}
                            setUndoRedo={setUndoRedo}
                            undoRedo={undoRedo}
                            canvasFront={canvasFront}
                            tab={tab}
                        />

                        <PreviewShirtBack
                            shirtColor={shirtColor}
                            image={product ? (product === "Man" ? backShirt : shirtColor === "white" ? backShirtWoman : backShirtWomanDark) : null}
                            textBack={textBack}
                            shirtPosition={shirtPosition}
                            imageUpload={photoBack}
                            productType={product}
                            setUndoRedo={setUndoRedo}
                            undoRedo={undoRedo}
                            canvasBack={canvasBack}
                            tab={tab}
                        />
                        <PreviewRightSleeve
                            shirtColor={shirtColor}
                            image={product ? (product === "Man" ? rightSleeveMen : rightSleeveWomen) : null}
                            textRight={textRightSleeve}
                            shirtPosition={shirtPosition}
                            imageUpload={rightSleevePhoto}
                            productType={product}
                            setUndoRedo={setUndoRedo}
                            undoRedo={undoRedo}
                            canvasRight={canvasRight}
                            tab={tab}
                        />
                        <PreviewLeftSleeve
                            shirtColor={shirtColor}
                            image={product ? (product === "Man" ? leftSleeveMen : leftSleeveWomen) : null}
                            textLeft={textLeftSleeve}
                            shirtPosition={shirtPosition}
                            imageUpload={leftSleevePhoto}
                            productType={product}
                            setUndoRedo={setUndoRedo}
                            undoRedo={undoRedo}
                            canvasLeft={canvasLeft}
                            tab={tab}
                        />
                        <div className={"lg:w-[4rem] w-full absolute lg:top-[20%] lg:bottom-5 bottom-0 flex lg:block lg:right-5 bg-white rounded-[0.25rem] lg:z-[9999] h-fit"}>
                            <div
                                className={clsx(
                                    `p-[0.6rem] cursor-pointer flex justify-center items-center`,
                                    shirtPosition === "front" ? "border border-mainColor" : "border-gray border",
                                    shirtPosition === "back" && "lg:border-b-0 lg:border-r border-r-0"
                                )}
                                onClick={() => changePositionShirt("front")}
                            >
                                <img
                                    src={product === "Man" ? frontShirt : shirtColor === "white" ? frontShirtWoman : frontShirtWomanDark}
                                    alt={"front-logo"}
                                    className={clsx(
                                        `w-full`,
                                        shirtColor && `bg-${shirtColor === "red" ? "rgbRed" : shirtColor === "blue" ? "rgbBlue" : shirtColor === "green" ? "rgbGreen" : shirtColor === "black" ? "rgbBlack" : shirtColor === "white" ? "rgbDarkWhite" : shirtColor}`,
                                    )}
                                />
                            </div>
                            <div
                                className={clsx(
                                    `p-[0.6rem] cursor-pointer flex justify-center items-center`,
                                    shirtPosition === "back" ? "border border-mainColor" : "lg:border-b lg:border-l lg:border-r lg:border-t-0 border-t border-b border-r border-gray",
                                    shirtPosition === "right" && "lg:border-b-0 border-r-0"
                                )}
                                onClick={() => changePositionShirt("back")}
                            >
                                <img
                                    src={product === "Man" ? backShirt : backShirtWomanDark}
                                    alt={"back-logo"}
                                    className={clsx(
                                        `w-full`,
                                        shirtColor && `bg-${shirtColor === "red" ? "rgbRed" : shirtColor === "blue" ? "rgbBlue" : shirtColor === "green" ? "rgbGreen" : shirtColor === "black" ? "rgbBlack" : shirtColor === "white" ? "rgbDarkWhite" : shirtColor}`,
                                    )}
                                />
                            </div>
                            <div
                                className={clsx(
                                    `p-[0.6rem] cursor-pointer flex justify-center items-center`,
                                    shirtPosition === "right" ? "border border-mainColor" : "lg:border-l lg:border-r lg:border-t-0 border-gray lg:border-b border-t border-b border-r",
                                    shirtPosition === "left" && "lg:border-b-0 lg:border-r border-r-0"
                                )}
                                onClick={() => changePositionShirt("right")}
                            >
                                <img
                                    src={product === "Man" ? rightSleeveMen : rightSleeveWomen}
                                    alt={"back-logo"}
                                    className={clsx(
                                        `w-full`,
                                        shirtColor && `bg-${shirtColor === "red" ? "rgbRed" : shirtColor === "blue" ? "rgbBlue" : shirtColor === "green" ? "rgbGreen" : shirtColor === "black" ? "rgbBlack" : shirtColor}`,
                                    )}
                                />
                            </div>
                            <div
                                className={clsx(
                                    `p-[0.6rem] cursor-pointer flex justify-center items-center`,
                                    shirtPosition === "left" ? "border border-mainColor" : "lg:border-b lg:border-l border-r lg:border-t-0 border-gray border-t border-b"
                                )}
                                onClick={() => changePositionShirt("left")}
                            >
                                <img
                                    src={product === "Man" ? leftSleeveMen : leftSleeveWomen}
                                    alt={"back-logo"}
                                    className={clsx(
                                        `w-full`,
                                        shirtColor && `bg-${shirtColor === "red" ? "rgbRed" : shirtColor === "blue" ? "rgbBlue" : shirtColor === "green" ? "rgbGreen" : shirtColor === "black" ? "rgbBlack" : shirtColor}`,
                                    )}
                                />
                            </div>
                        </div>
                    </> :
                    <div
                        className={"middle-absolute-front flex items-center justify-center relative overflow-hidden"}
                    >
                        <div className="lds-ring lds-ring-shirt">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                }
            </div>
            {!loaderProduct && ((!isWide && tab === "preview") || isWide) &&
                <span
                    className={clsx(
                        `no-highlight absolute lg:bottom-5 cursor-pointer bg-mainColor text-gray-800 active:bg-gray-100 text-xs font-bold uppercase px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none`,
                        isWide ? " bottom-[5.5rem] right-5" : "top-[5.5rem] right-5 lg:right-5"
                    )}

                onClick={addToCartFunction}
                >
                  Add to cart
                </span>
            }
            <Modal
                show={showPreviewModal && !product}
                typeModal={"center"}
                className={"relative !p-0 !pb-0"}
                close={() => navigate('/')}
                loading={loader}
            >
                <div
                    className={clsx(
                        `text-center text-[20px] w-full`,
                        !loader ? "visible" : "invisible",
                    )}
                >
                    <div className={"bg-mainColor text-[#212221] text-[25px] text-ComingSoon text-center p-2 border-b-[5px] border-[#b9b9ba] font-bold"}>
                        choose your product
                    </div>
                    <div className={"flex lg:gap-[2rem]  gap-[1.2rem] p-1 bg-[#f6f6f6] justify-center"}>
                        <div className={"py-[1rem] cursor-pointer product-box relative"} onClick={() => { setProduct("Man");setShirtPosition("front");setShowModalPreview(false) }}>
                           <div>
                               <div className={"relative bg-mainColor p-[16px]"}>
                                   <div className={"box-preview-top"}></div>
                               </div>
                               <img src={manShirt} alt={"man-product"}  className={"w-full max-w-[300px]"} onLoad={() => setLoader(false)} />
                               <div className={"bg-mainColor px-[1.5rem] pt-[1.5rem] pb-[1rem] relative"}>
                                   <div className={"box-preview-bottom"}></div>
                                   <p className={"text-[#212221] lg:text-[25px] text-[13px] font-bold text-ComingSoon mt-[15px]"}>
                                       MANS T-SHIRT
                                   </p>
                               </div>
                           </div>
                        </div>
                        <div className={"py-[1rem] cursor-pointer product-box relative"} onClick={() => { setProduct("Woman");setShirtPosition("front");setShowModalPreview(false) }}>
                            <div>
                                <div className={"relative bg-mainColor p-[16px]"}>
                                    <div className={"box-preview-top"}></div>
                                </div>
                                <img src={womanShirt} alt={"woman-product"} className={"w-full max-w-[300px]"} />
                                <div className={"bg-mainColor px-[1.5rem] pt-[1.5rem] pb-[1rem] relative"}>
                                    <div className={"box-preview-bottom"}></div>
                                    <p className={"text-[#212221] lg:text-[25px] text-[13px] font-bold text-ComingSoon mt-[15px]"}>
                                        WOMENS T-SHIRT
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {loader &&
                    <div
                        className={"middle-absolute-front flex items-center justify-center relative overflow-hidden"}
                    >
                        <div className="lds-ring lds-ring-shirt">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                }
            </Modal>
        </div>
    )
}