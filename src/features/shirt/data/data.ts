export const leftMenuData = [
    {
        label: "Preview",
        icon: "shirt",
        value: "preview",
        mobile:true
    },
    {
        label: "Info",
        icon: "info",
        value: "info"
    },
    {
        label: "Text",
        icon: "font",
        value: "text"
    },
    {
        label: "Image",
        icon: "image",
        value: "image"
    },
    {
        label: "Notes",
        icon: "file-lines",
        value: "notes"
    },
]

export const shirtColorData = [
    {
        color: "blackShirt"
    },
    {
        color: "purpleShirt"
    },
    {
        color: "purpleDarkShirt"
    },
    {
        color: "blueShirt"
    },
    {
        color: "azureBlue"
    },
    {
        color: "blueSkyShirt"
    },
    {
        color: "pinkShirt"
    },
    {
        color: "whiteShirt"
    },
    {
        color: "grayShirt"
    },
    {
        color: "limeShirt"
    },
    {
        color: "greenShirt"
    },
    {
        color: "yellowShirt"
    },
    {
        color: "yellowDarkShirt"
    },
    {
        color: "orangeShirt"
    },
    {
        color: "redShirt"
    },
    {
        color: "redDarkShirt"
    },
]

export const sizeShirtData = [
    {
        value: "S"
    },
    {
        value: "M"
    },
    {
        value: "L"
    }, {
        value: "XL"
    },
    {
        value: "2XL"
    },
]

export const productShirtData = [
    {
        value: "Man"
    },
    {
        value: "Woman"
    },
]

export const effectImageData = [
    {
        label: "Default",
        value: "default"
    },
    {
        label: "Grayscale",
        value: "grayScale"
    },
    {
        label: "Sepia",
        value: "sepia"
    },
]