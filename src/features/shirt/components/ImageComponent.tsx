import {FunctionComponent} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
export const ImageComponent:FunctionComponent<any> = props => {

    const {inputRef, handleFileUpload} = props

    const resetFileInput = () => {
        inputRef.current.value = null;
    };

    return (
        <div className={"min-h-[calc(100vh_-_68px)]"}>
            <div className={"bg-[#3f4652] text-[#dddd] text-center border-b-[1px] border-[#484f5a] p-3"}>
                Choose Your Photo
            </div>
            <div className={"p-[2rem]"}>
                <div className={"text-center text-[20px] w-full"}>
                    <div
                        onClick={() => inputRef?.current?.click()}
                        className={"relative border border-[#484f5a] border-dashed  cursor-pointer m-auto rounded-[4px] flex justify-center items-center text-[#ddd] p-5"}
                    >
                        <div>
                            <FontAwesomeIcon
                                icon={["fas", "image"]}
                            />
                            <span className={"block"}>Click here</span>
                            <input
                                ref={inputRef}
                                type="file"
                                accept="image/*"
                                className={"hidden"}
                                onChange={(e) => {handleFileUpload(e);resetFileInput()}}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}