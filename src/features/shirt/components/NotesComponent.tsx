import {FunctionComponent} from "react";
export const NotesComponent:FunctionComponent<any> = props => {

    const { setNotes,notes } = props

    return (
        <div className={"min-h-[calc(100vh_-_68px)]"}>
            <div className={"bg-[#3f4652] text-[#dddd] text-center border-b-[1px] border-[#484f5a] p-3"}>
                Notes
            </div>
            <div className={"text-center 2xl:p-[2rem] p-[1rem] text-[20px] w-full "}>
                <textarea
                    value={notes}
                    placeholder={"your message..."}
                    className={"w-full bg-[#323844] p-2 focus-none rounded-[5px] text-[#ddd]"}
                    onChange={(e) => setNotes(e.target.value)}
                    rows={7}
                />
            </div>
        </div>
    )
}