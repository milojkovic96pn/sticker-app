import {FunctionComponent, useEffect, useRef, useState} from "react";
import { HexColorPicker } from "react-colorful";
import clsx from "clsx";
import { useClickAway } from "react-use";
import styled from "styled-components";
import {ReactSelect} from "../../../components/elements/ReactSelect";
export const  TextComponent:FunctionComponent<any> = props => {

    const {text,setText,selectShape,setSelectShape } = props
    const [collapse, setCollapse] = useState(false);
    const ref = useRef(null);
    const [fontFamily, setFontFamily] = useState<string>("")

    const handleCollapse = () => {
        setCollapse(!collapse);
    };

    useClickAway(ref, () => {
        if (collapse) {
            setTimeout(() => {
                handleCollapse();
            }, 100);
        }
    });

    const [textValue,setTextValue] = useState<string>("")
    const [color,setColor] = useState<any>("#627a8573")

    const addText = (shape = "default") => {
       if(!textValue) return
        setText({value:textValue, positionX:60, positionY:20,color:color, shape:shape, fontSize:50,fontFamily:fontFamily})
        setFontFamily("")
        setTextValue("")
        setColor("#627a8573")
    }
    useEffect(() => {
        if(selectShape?.selectedShapeName) {
            let allText = [...text];
            let find: any = allText.find(
                (item) => item.value === selectShape?.selectedShapeName
            );
            if(find) {
                setTextValue(find.value)
                // setSize(find.size)
                setColor(find.color)
                setFontFamily(find.fontFamily)
            }
        } else {
            setTextValue("")
            setFontFamily("")
            setColor("#627a8573")
        }
    },[selectShape?.selectedShapeName,text])

    const Wrapper = styled.div<{ color:string}>`
          background:${(props) =>
                  props?.color ? props.color : "0px 0px 20px #0000000f"};
        `;

    const options = [
        { value: 'Fantasy', label: 'Fantasy', fontFamily:"fantasy" },
        { value: 'HoltwoodOneSC', label: 'Holtwood', fontFamily: "HoltwoodOneSC" },
        { value: 'ComingSoon', label: 'ComingSoon', fontFamily: "ComingSoon" },
    ];

    return (
        <div className={"lg:pb-0 pb-[2rem] min-h-[calc(100vh_-_68px)] bg-[#3f4652]"}>
            <div className={"bg-[#3f4652] text-mainColor text-center border-b-[1px] border-[#484f5a] p-3"}>
                Design Text
            </div>
            <div className={"text-center 2xl:p-[2rem] p-[1rem] text-[20px] w-full"}>
                <input
                    type={"text"}
                    placeholder={"add text..."}
                    className={"w-full p-2 bg-[#323844] text-[#ddd] focus-none  rounded-[5px]"}
                    onChange={(e) => setTextValue(e.target.value)}
                    value={textValue}
                />
            </div>
            <div className={"2xl:px-[2rem] px-[1rem]"}>
                <div className={"lg:flex border-b-[1px] border-t-[1px] border-[#484f5a] lg:py-[18px] py-[1rem]"}>
                    <div className={"flex lg:justify-start justify-center items-center lg:pb-0 pb-[10px]"}>
                        <span className={"text-[#ddd] m-"}>Color:</span>
                    </div>
                    <div className={"flex lg:justify-end justify-center w-full relative"}  ref={ref}>
                        <Wrapper
                            onClick={() => handleCollapse()}
                            className={clsx(
                                `h-[38px] w-[38px] border-[2px] border-[#484f5a] lg:mr-3 cursor-pointer rounded-[38px] hover-shadow relative`,
                            )}
                            color={color}
                        ></Wrapper>
                        {collapse && (
                            <div className={"absolute pt-3 top-[100%] right-0 z-40"}>
                                <HexColorPicker color={color} onChange={setColor} />
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <div className={"2xl:px-[2rem] px-[1rem]"}>
                <div className={"lg:flex border-b-[1px] border-t-[1px] border-[#484f5a] lg:py-[18px] py-[1rem]"}>
                    <div className={"flex lg:justify-start justify-center items-center lg:w-6/12 lg:pb-0 pb-[10px]"}>
                        <span className={"text-[#ddd] m-"}>Font Family:</span>
                    </div>
                    <div className={"flex lg:justify-end justify-center w-full relative"}>
                        <ReactSelect
                            padding={1}
                            onChange={setFontFamily}
                            value={fontFamily}
                            options={options ?
                            options.map((item:any) => {
                                return {
                                    value: item.value,
                                    label: (
                                        <span className={`text-${item.fontFamily}`}>
                                            { item.label}
                                        </span>
                                    )
                                }
                                }) : [{ value: "0", label: "No data" }]
                            }
                        />
                    </div>
                </div>
            </div>
            <button
                disabled={!textValue}
                className=" m-auto block w-[140px] text-center mt-5 cursor-pointer bg-mainColor font-bold
            text-xs  uppercase px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none text-[#272c33]"
                onClick={() => addText()}
            >
               Add Text
            </button>
        </div>
    )
}