import React, {FunctionComponent, useEffect, useRef, useState} from "react";
import clsx from "clsx";
import {fabric} from "fabric";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useMedia} from "react-use";
import {HexColorPicker} from "react-colorful";
import styled from "styled-components";
import deleteIcon from "../../../../assets/delete-fabric.svg";
import {ReactSelect} from "../../../../components/elements/ReactSelect";

export const PreviewShirtBack:FunctionComponent<any> = props => {

    const {shirtColor,image,textBack,shirtPosition,imageUpload,productType,undoRedo,setUndoRedo,canvasBack,tab} = props
    const [loader,setLoader] = useState(true)

    useEffect(() => {
        if(canvasBack && !loader) {
            canvasBack.current = initCanvas();

            // canvasBack.current.on("mouse:over", () => {
            //     console.log('hello')
            // }, []);

            return () => {
                canvasBack.current.dispose();
                canvasBack.current = null;
            };
        }
    }, [loader]);

    const handleFileUpload = (path:any) => {
        fabric.Image.fromURL(path, function(img) {
            img.set({ left:isWide ? 50 : 0, top: isWide ? 50 : 0});
            img.scaleToWidth(isWide ? 200 : 100);
            canvasBack.current.add(img);
        });
    };

    useEffect(() => {
        if(imageUpload) {
            handleFileUpload(imageUpload.path)
        }
    }, [imageUpload]);

    const isWide = useMedia("(min-width: 1024px)");
    const initCanvas = () => (
        new fabric.Canvas('canvas-back', {
            height: !isWide ? productType === "Man" ? 355 : 280 : 650,
            width: !isWide ? productType === "Man" ? 175 : 130 : 310,
            backgroundColor: "#ffffff0f",
            selection: false,
            renderOnAddRemove: true,
        })
    );

    const addTextToCanvas = (item:any) => {
        let textBox = new fabric.IText(item.value, {
            left: item.positionY,
            top:  item.positionX,
            fontSize: 20,
            fontStyle: "normal",
            fill: item.color,
            fontFamily: item.fontFamily ? item.fontFamily : "fantasy",
        });
        canvasBack.current.add(textBox);
    }

    useEffect(() => {
        if(textBack) {
            addTextToCanvas(textBack)
        }
    }, [textBack])

    const undo = () => {
        if(canvasBack) {
            if (canvasBack.current._objects.length > 0) {
                let allObjects = [...undoRedo]
                allObjects.push(canvasBack.current._objects.pop())
                setUndoRedo(allObjects)
            }
            canvasBack.current.renderAll();
        }
    };

    const redo = () => {
        if (undoRedo.length > 0) {
            canvasBack.current.add(undoRedo.pop());
        }
    };


    const changeColor = (color:string) => {
        if(!canvasBack?.current?.getActiveObject() || canvasBack?.current?.getActiveObject()?.type !== "i-text") return;
        canvasBack?.current?.getActiveObject().set("fill",color)
        canvasBack.current.renderAll();
    }

    const changeFontFamily = (font:string) => {
        if(!canvasBack?.current?.getActiveObject() || canvasBack?.current?.getActiveObject()?.type !== "i-text") return;
        canvasBack?.current?.getActiveObject().set("fontFamily",font)
        canvasBack.current.renderAll();
        setFontFamily(font)
    }


    const [collapse, setCollapse] = useState(false);
    const [collapseColor, setCollapseColor] = useState(false);

    const ref = useRef(null);
    const [color,setColor] = useState<any>("#627a8573")

    const handleCollapse = () => {
        if(!canvasBack?.current?.getActiveObject() || canvasBack?.current?.getActiveObject()?.type !== "i-text") return;
        setCollapse(!collapse);
    };

    const handleCollapseColor = () => {
        if(!canvasBack?.current?.getActiveObject() || canvasBack?.current?.getActiveObject()?.type !== "i-text") return;
        setCollapseColor(!collapseColor);
    };

    const Wrapper = styled.div<{ color:string}>`
          background:${(props) =>
        props?.color ? props.color : "0px 0px 20px #0000000f"};
        `;

    const options = [
        { value: 'Fantasy', label: 'Fantasy', fontFamily:"fantasy" },
        { value: 'HoltwoodOneSC', label: 'Holtwood', fontFamily: "HoltwoodOneSC" },
        { value: 'ComingSoon', label: 'ComingSoon', fontFamily: "ComingSoon" },
    ];

    const [fontFamily, setFontFamily] = useState<string>("")

    const [b,setB] = useState(false)
    useEffect(() => {
        if(canvasBack?.current?._objects.length) {
            setB(true)
        } else {
            setB(false)
        }
    },[textBack,canvasBack?.current?._objects])

    return (
       <>
           {shirtPosition === "back" &&
               <div
                   className={clsx(
                       `absolute lg:left-4 left-0 lg:top-4 top-0 text-[#3f4652] z-[99] lg:mt-0 lg:ml-0 mt-5 ml-3`,
                   )}
               >
                   <div>
                       <span onClick={undo} className={"cursor-pointer"}>
                              <FontAwesomeIcon
                                  icon={["fas", "arrow-rotate-left"]}
                                  className={"mr-1"}
                              />
                                  Undo
                            </span>
                       <span className={"ml-2 cursor-pointer"} onClick={redo}>
                              <FontAwesomeIcon
                                  icon={["fas", "arrow-rotate-right"]}
                                  className={"mr-1"}
                              />
                              Redo
                        </span>
                   </div>
                   {canvasBack?.current?._objects.length ?
                       <div className={"mt-3"}>
                                <span
                                    onClick={() => handleCollapse()}
                                    className={clsx(
                                        `no-highlight cursor-pointer bg-[#3f4652] text-white active:bg-gray-100 text-xs uppercase px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none`,
                                    )}
                                >
                                <FontAwesomeIcon
                                    icon={["fas", "edit"]}
                                    className={"mr-1"}
                                />
                                Update text style
                             </span>
                       </div> : null
                   }
                   {collapse && (
                       <div className={"absolute top-[100%] left-0 z-[99] bg-[#3f4652] rounded border border-white"}>
                           <div className={"absolute -right-8 w-[25px] cursor-pointer"} onClick={() => {setCollapse(false);setCollapseColor(false)}}>
                               <img src={deleteIcon} alt={"close"} />
                           </div>
                           <div className={"flex items-center justify-start w-full relative gap-5 p-3"}>
                               <span className={"text-white"}>Color:</span>
                               <Wrapper
                                   onClick={() => handleCollapseColor()}
                                   className={clsx(
                                       `h-[38px] w-[38px] border-[2px] border-[#484f5a] cursor-pointer rounded-[38px] hover-shadow relative`,
                                   )}
                                   color={color}
                               ></Wrapper>
                               {collapseColor && (
                                   <div className={"absolute pt-3 top-[100%] right-0 z-40"}>
                                       <HexColorPicker color={color} onChange={(newColor:string) => changeColor(newColor)} />
                                   </div>
                               )}
                           </div>
                           <div className={"flex items-center justify-center w-full relative gap-5 p-3"}>
                               <span className={"text-white"}>Font:</span>
                               <ReactSelect
                                   isClearable={false}
                                   padding={1}
                                   onChange={(font) => changeFontFamily(font)}
                                   value={fontFamily}
                                   options={options ?
                                       options.map((item:any) => {
                                           return {
                                               value: item.value,
                                               label: (
                                                   <span className={`text-${item.fontFamily}`}>
                                            { item.label}
                                        </span>
                                               )
                                           }
                                       }) : [{ value: "0", label: "No data" }]
                                   }
                               />
                           </div>
                       </div>
                   )}
               </div>
           }
           <div
               className={clsx(
                   `lg:absolute top-0 min-h-[100%]`,
                   shirtPosition === "back" && (isWide || (!isWide && tab === "preview")) ?  "visible lg:absolute" : "invisible absolute",
                   productType === "Man" ? "lg:w-[750px] w-full" : "lg:w-[1000px] w-full"
               )}
           >
               <div
                   className={clsx(
                       `lg:absolute top-0 relative lg:mt-0`,
                       shirtPosition === "back" && (isWide || (!isWide && tab === "preview")) ? "visible" : "invisible",
                   )}
               >
                   <img
                       src={image}
                       alt={"back-preview"}
                       className={clsx(
                           ``,
                           shirtColor && `bg-${shirtColor}`,
                           productType === "Woman" ? "lg:max-w-[1000px] lg:-mt-[5rem]" : ""
                       )}
                       onLoad={() => setLoader(false)}
                   />
                   {!loader ?
                       <div
                           className={clsx(
                               `lg:w-[310px] lg:h-[650px] z-[9] border border-dashed`,
                               shirtColor === "whiteShirt" || shirtColor === "yellowShirt" || shirtColor === "yellowDarkShirt" ? "border-[#3f4652]" : "border-white",
                               productType === "Man" ? "middle-absolute-back h-[355px] w-[175px]" : "lg:middle-absolute-back-women middle-absolute-back-mobile h-[280px] w-[130px]"
                           )}
                       >
                           <canvas id="canvas-back" className={"z-[999]"} />
                       </div> : null
                   }
               </div>
           </div>
       </>
    )
}