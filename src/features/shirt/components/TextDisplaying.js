import React, { Component } from "react";
import {Text, TextPath} from "react-konva";
import PropTypes from "prop-types";

class TextDisplaying extends Component {
    state = {
        text: null,
        edited: true,
        setPositionText: null
    };
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.text !== prevState.text) {
            return { text: nextProps.text };
        } else if (nextProps.textColor !== prevState.textColor) {
            return { textColor: nextProps.textColor };
        } else if (nextProps.setPositionText !== prevState.setPositionText) {
            return { setPositionText: nextProps.setPositionText };
        }
        return null;
    }
    onDragEnd = () => {
        this.setState({
            edited: true
        });
    };
    onDragMove = env => {
        const txtPos = env.target._lastPos;
        this.setState({
            txtPos
        });
    };
    render() {
        const textSettings = {
            name: this.state.text.value,
            text: this.state.text.value,
            fill: this.state.text.color,
            onDragEnd: this.onDragEnd,
            onTransformEnd: this.onDragEnd,
            onDragMove: this.onDragMove,
            draggable: true,
            x: this.state.text.positionX,
            y: this.state.text.positionY,
            ...(!this.state.edited && { scale: { x: 3, y: 3 } })
        };
        if(this.state.text.shape === "default") {
            return <Text {...textSettings} fontSize={this.state.text.fontSize || 100} fontFamily={this.state.text.fontFamily ? this.state.text.fontFamily : "fantasy"} />;
        } else {
            return  <TextPath {...textSettings} fontFamily={this.state.text.fontFamily ? this.state.text.fontFamily : "fantasy"} x={250} y={250} fill={this.state.text.color} fontSize={30} text={this.state.text.value} data={this.state.text.shape} letterSpacing={6} />
        }
    }
}

TextDisplaying.propTypes = {
    text: PropTypes.any,
};

export default TextDisplaying;
