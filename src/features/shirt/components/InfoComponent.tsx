import {shirtColorData, sizeShirtData} from "../data/data";
import clsx from "clsx";
import {FunctionComponent} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export const InfoComponent:FunctionComponent<any> = props => {

    const { changeColorShirt,activeColor,chooseSize,size,setQuantity,quantity,productType,setProduct,setLoaderProduct } = props

    const counterMin = () => {
        if(quantity < 2) return
        setQuantity(quantity - 1)
    }

    return (
        <div className={"lg:pb-0 pb-[10rem]"}>
            <div className={"text-mainColor text-center border-b-[1px] border-t-[1px] border-[#484f5a] p-3"}>
                Choose Your Product Color
            </div>
            <div className={"grid grid-cols-7 p-[0.5rem] justify-center items-center"}>
                {shirtColorData.map((color,index) =>
                    <div
                        key={index}
                        onClick={() => changeColorShirt(color.color)}
                        className={clsx(
                            `lg:mb-3 h-[38px] w-[38px] bg-${color.color} m-auto border-[2px] border-[#484f5a] cursor-pointer rounded-[38px] hover-shadow relative lg:mt-0 mt-1`,
                            activeColor(color.color, "shirt") && "single-color-active"
                        )}
                    >
                        {activeColor(color.color, "shirt")}
                    </div>
                )}
            </div>
            <div className={"text-[#dddd] text-center border-b-[1px] border-t-[1px] border-[#484f5a] p-3"}>
                Choose Your Size
            </div>
            <div className={"flex lg:p-[2rem] p-[1rem] text-[20px] justify-center"}>
                {sizeShirtData.map((sizeItem,index) =>
                    <span
                        key={index}
                        onClick={() => chooseSize(sizeItem.value)}
                        className={clsx(
                            `pl-[1rem] pr-[1rem] [&:not(:last-child)]:border-r-[1px] border-[#484f5a] relative cursor-pointer text-[#ddd]`,
                            size === sizeItem.value && "size-active"
                        )}
                    >
                      {sizeItem.value}
                   </span>
                )}
            </div>
            <div className={"text-mainColor text-center border-b-[1px] border-t-[1px] border-[#484f5a] p-3"}>
                Quantity
            </div>
            <div className={"text-center p-[2rem] text-[20px] w-full flex gap-4 justify-center no-highlight"}>
                <div
                    className={"w-[34px] h-[34px] border border-[rgba(255,255,255,0.25)] rounded-[50%] text-[#ccc] hover-shadow cursor-pointer no-highlight"}
                    onClick={() => counterMin()}
                >
                    <FontAwesomeIcon
                        icon={["fas", "minus"]}
                    />
                </div>
                <input
                    value={quantity}
                    type={"text"}
                    min={1}
                    className={"w-[100px] text-center bg-[#323844] text-[#ddd] h-[34px] focus-none rounded-[5px] no-highlight"}
                    onChange={(e) => setQuantity(e.target.value)}
                />
                <div
                    className={"w-[34px] h-[34px] border border-[rgba(255,255,255,0.25)] rounded-[50%] text-[#ccc] hover-shadow cursor-pointer"}
                    onClick={() => setQuantity(quantity + 1)}
                >
                    <FontAwesomeIcon
                        icon={["fas", "plus"]}
                    />
                </div>
            </div>
        </div>
    )
}