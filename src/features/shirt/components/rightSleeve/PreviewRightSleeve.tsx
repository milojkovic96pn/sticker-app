import React, {FunctionComponent, useEffect, useRef, useState} from "react";
import clsx from "clsx";
import {fabric} from "fabric";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useMedia} from "react-use";
import {HexColorPicker} from "react-colorful";
import deleteIcon from "../../../../assets/delete-fabric.svg";
import {ReactSelect} from "../../../../components/elements/ReactSelect";
import styled from "styled-components";

export const PreviewRightSleeve:FunctionComponent<any> = props => {

    const {shirtColor,image,textRight,shirtPosition,imageUpload,productType,undoRedo,setUndoRedo,canvasRight,tab} = props

    const [loader,setLoader] = useState(true)

    useEffect(() => {
        if(canvasRight && !loader) {
            canvasRight.current = initCanvas();

            return () => {
                canvasRight.current.dispose();
                canvasRight.current = null;
            };
        }
    }, [loader]);

    const handleFileUpload = (path:any) => {
        fabric.Image.fromURL(path, function(img) {
            img.set({ left:isWide ? 50 : 0, top: isWide ? 50 : 0});
            img.scaleToWidth(isWide ? 200 : 100);
            canvasRight.current.add(img);
        });
    };

    useEffect(() => {
        if(imageUpload) {
            handleFileUpload(imageUpload.path)
        }
    }, [imageUpload]);

    const isWide = useMedia("(min-width: 1024px)");
    const initCanvas = () => (
        new fabric.Canvas('canvas-right', {
            height: !isWide ? 150 : 235,
            width: !isWide ? 130 : 235,
            backgroundColor: "#ffffff0f",
            selection: false,
            renderOnAddRemove: true,
        })
    );

    const addTextToCanvas = (item:any) => {
        let textBox = new fabric.IText(item.value, {
            left: item.positionY,
            top:  item.positionX,
            fontSize: 20,
            fontStyle: "normal",
            fill: item.color,
            fontFamily: item.fontFamily ? item.fontFamily : "fantasy",
        });
        canvasRight.current.add(textBox);
    }

    useEffect(() => {
        if(textRight) {
            addTextToCanvas(textRight)
        }
    }, [textRight])

    const undoRight = () => {
        if(canvasRight) {
            if (canvasRight.current._objects.length > 0) {
                let allObjects = [...undoRedo]
                allObjects.push(canvasRight.current._objects.pop())
                setUndoRedo(allObjects)
            }
            canvasRight.current.renderAll();
        }
    };

    const redoRight = () => {
        if (undoRedo.length > 0) {
            canvasRight.current.add(undoRedo.pop());
        }
    };

    const changeColor = (color:string) => {
        if(!canvasRight?.current?.getActiveObject() || canvasRight?.current?.getActiveObject()?.type !== "i-text") return;
        canvasRight?.current?.getActiveObject().set("fill",color)
        canvasRight.current.renderAll();
    }

    const changeFontFamily = (font:string) => {
        if(!canvasRight?.current?.getActiveObject() || canvasRight?.current?.getActiveObject()?.type !== "i-text") return;
        canvasRight?.current?.getActiveObject().set("fontFamily",font)
        canvasRight.current.renderAll();
        setFontFamily(font)
    }


    const [collapse, setCollapse] = useState(false);
    const [collapseColor, setCollapseColor] = useState(false);

    const [color,setColor] = useState<any>("#627a8573")

    const handleCollapse = () => {
        if(!canvasRight?.current?.getActiveObject() || canvasRight?.current?.getActiveObject()?.type !== "i-text") return;
        setCollapse(!collapse);
    };

    const handleCollapseColor = () => {
        if(!canvasRight?.current?.getActiveObject() || canvasRight?.current?.getActiveObject()?.type !== "i-text") return;
        setCollapseColor(!collapseColor);
    };

    const Wrapper = styled.div<{ color:string}>`
          background:${(props) =>
        props?.color ? props.color : "0px 0px 20px #0000000f"};
        `;

    const options = [
        { value: 'Fantasy', label: 'Fantasy', fontFamily:"fantasy" },
        { value: 'HoltwoodOneSC', label: 'Holtwood', fontFamily: "HoltwoodOneSC" },
        { value: 'ComingSoon', label: 'ComingSoon', fontFamily: "ComingSoon" },
    ];

    const [fontFamily, setFontFamily] = useState<string>("")

    const [b,setB] = useState(false)
    useEffect(() => {
        if(canvasRight?.current?._objects.length) {
            setB(true)
        } else {
            setB(false)
        }
    },[textRight,canvasRight?.current?._objects])

    return (
        <>
            {shirtPosition === "right" &&
                <div
                    className={clsx(
                        `absolute lg:left-4 left-0 lg:top-4 top-0 text-[#3f4652] z-[99] lg:mt-0 lg:ml-0 mt-5 ml-3`,
                    )}
                >
                    {undoRedo.length ?
                        <div>
                            <span onClick={undoRight} className={"cursor-pointer"}>
                          <FontAwesomeIcon
                              icon={["fas", "arrow-rotate-left"]}
                              className={"mr-1"}
                          />
                          Undo
                            </span>
                                <span className={"ml-2 cursor-pointer"} onClick={redoRight}>
                                  <FontAwesomeIcon
                                      icon={["fas", "arrow-rotate-right"]}
                                      className={"mr-1"}
                                  />
                                  Redo
                            </span>
                        </div> : null
                    }
                    <div className={"mt-3"}>
                                <span
                                    onClick={() => handleCollapse()}
                                    className={clsx(
                                        `no-highlight cursor-pointer bg-[#3f4652] text-white active:bg-gray-100 text-xs uppercase px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none`,
                                    )}
                                >
                                <FontAwesomeIcon
                                    icon={["fas", "edit"]}
                                    className={"mr-1"}
                                />
                                Update text style
                             </span>
                    </div>
                    {collapse && (
                        <div className={"absolute top-[100%] left-0 z-[99] bg-[#3f4652] rounded border border-white"}>
                            <div className={"absolute -right-8 w-[25px] cursor-pointer"} onClick={() => {setCollapse(false);setCollapseColor(false)}}>
                                <img src={deleteIcon} alt={"close"} />
                            </div>
                            <div className={"flex items-center justify-start w-full relative gap-5 p-3"}>
                                <span className={"text-white"}>Color:</span>
                                <Wrapper
                                    onClick={() => handleCollapseColor()}
                                    className={clsx(
                                        `h-[38px] w-[38px] border-[2px] border-[#484f5a] cursor-pointer rounded-[38px] hover-shadow relative`,
                                    )}
                                    color={color}
                                ></Wrapper>
                                {collapseColor && (
                                    <div className={"absolute pt-3 top-[100%] right-0 z-40"}>
                                        <HexColorPicker color={color} onChange={(newColor:string) => changeColor(newColor)} />
                                    </div>
                                )}
                            </div>
                            <div className={"flex items-center justify-center w-full relative gap-5 p-3"}>
                                <span className={"text-white"}>Font:</span>
                                <ReactSelect
                                    isClearable={false}
                                    padding={1}
                                    onChange={(font) => changeFontFamily(font)}
                                    value={fontFamily}
                                    options={options ?
                                        options.map((item:any) => {
                                            return {
                                                value: item.value,
                                                label: (
                                                    <span className={`text-${item.fontFamily}`}>
                                            { item.label}
                                        </span>
                                                )
                                            }
                                        }) : [{ value: "0", label: "No data" }]
                                    }
                                />
                            </div>
                        </div>
                    )}
                </div>
            }
            <div
                className={clsx(
                    `top-0 min-h-[100%] lg:w-[750px] w-full`,
                    shirtPosition === "right" && (isWide || (!isWide && tab === "preview")) ?  "visible lg:absolute" : "invisible absolute",
                )}
            >

                <div
                    className={clsx(
                        `top-0 relative lg:mt-0 mt-[5rem]`,
                        shirtPosition === "right" && (isWide || (!isWide && tab === "preview")) ? "visible" : "invisible"
                    )}
                >
                    <img
                        src={image}
                        alt={"right-preview"}
                        className={clsx(
                            ``,
                            shirtColor && `bg-${shirtColor}`,
                        )}
                        onLoad={() => setLoader(false)}
                    />
                    {!loader ?
                        <div
                            className={clsx(
                                `lg:w-[235px] lg:h-[235px] w-[130px] h-[150px] z-[9] border border-dashed`,
                                shirtColor === "whiteShirt" || shirtColor === "yellowShirt" || shirtColor === "yellowDarkShirt" ? "border-[#3f4652]" : "border-white",
                                productType === "Man" ? "middle-absolute-rSleeve-men" : "middle-absolute-rSleeve-women"
                            )}
                        >
                            <canvas id="canvas-right" className={"z-[999]"} />
                        </div> : null
                    }
                </div>
            </div>
        </>
    )
}