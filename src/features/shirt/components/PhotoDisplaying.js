import React, { Component } from "react";
import Konva from "konva";
import { Image } from "react-konva";
import PropTypes from "prop-types";
class LogoDisplaying extends Component {
    state = {
        image: undefined,
        logoEffect: undefined,
        imgPos: { x: 60, y: 180 },
        edited: false,
        imgWidth: undefined,
        imgHeight: undefined
    };
    static getDerivedStateFromProps(nextProps, prevState) {
        const image = new window.Image();
        const { imgSrc, logoEffect, imgWidth, imgHeight} = nextProps;
        image.src = imgSrc;
        return { image, logoEffect,imgWidth,imgHeight};
    }
    componentDidMount() {
        this.renderIMG();
    }
    componentDidUpdate(prevProps, prevState) {
        this.myImage.cache();
        this.myImage.getLayer().draw();
    }

    renderIMG() {
        const image = new window.Image();
        image.src = this.state.image;
        image.onload = () => {
            this.setState(
                {
                    image
                },
                () => {
                    this.myImage.cache();
                    this.myImage.getLayer().draw();
                }
            );
        };
    }

    onDragEnd = () => {
        this.setState({
            edited: true
        });
    };

    onDragMove = env => {
        const imgPos = env.target._lastPos;
        this.setState({
            imgPos
        });
    };

    render() {

        const { imgPos, logoEffect, imgWidth,imgHeight } = this.state;
        const filters =
            logoEffect === "grayScale"
                ? [Konva.Filters.Grayscale]
                : logoEffect === "sepia"
                    ? [Konva.Filters.Sepia]
                    : null;
        const imageSettings = {
            filters,
            name: "Logo",
            image: this.state.image,
            onTransformEnd: this.onDragEnd,
            onDragMove: this.onDragMove,
            draggable: true,
            ...imgPos,
        };

        return (
            <Image
                {...imageSettings}
                ref={node => {
                    this.myImage = node;
                }}
                width={imgWidth  === 500 ? 825 : imgWidth < 500 ? imgWidth : 350}
                height={imgHeight === 500 ? 820 : imgWidth < 500 ? imgWidth : 350}
                scaleX={0.6}
                scaleY={0.6}
                x={imgWidth && 0}
                y={imgWidth && 0}
            />
        );
    }
}

LogoDisplaying.propTypes = {
    imgSrc: PropTypes.string,
    logoEffect: PropTypes.string,
    imgWidth: PropTypes.number,
    imgHeight: PropTypes.number
};

export default LogoDisplaying;
