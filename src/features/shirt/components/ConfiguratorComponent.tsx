import {FunctionComponent} from "react";
import {leftMenuData} from "../data/data";
import clsx from "clsx";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {IconProp} from "@fortawesome/fontawesome-svg-core";
import {InfoComponent} from "./InfoComponent";
import {TextComponent} from "./TextComponent";
import {ImageComponent} from "./ImageComponent";
import {NotesComponent} from "./NotesComponent";
import {useMedia} from "react-use";

export const ConfiguratorComponent:FunctionComponent<any> = props => {

    const { tab,changeTab,size,inputRef,changeColorShirt,activeColor, chooseSize,setQuantity,quantity,changeColorText,setSelectShape,setLoaderProduct,
        imageUpload,notes,setNotes,handleFileUpload,setText,logoEffects,changeEffect,handleFileDelete,text,selectShape,productType,setProduct} = props

    const isWide = useMedia("(min-width: 1024px)");

    return (
        <div
            className={clsx(
                `lg:h-auto h-full 2xl:min-w-[500px] md:min-w-[400px]`,
                tab !== "preview" && "lg:pb-0 pb-[70px]"
            )}
        >
            <div className={"lg:flex h-full w-full"}>
                <div className={"bg-[#272c33] text-[#b7b7b7] fixed lg:static bottom-0 w-full lg:w-auto z-50"}>
                    <div className={"h-full border-r-[1px] border-[#484f5a] lg:w-[5rem] lg:block flex items-center justify-center"}>
                        {leftMenuData.filter((item:any) => isWide ? !item.mobile : item).map((tabItem,index) =>
                            <div
                                key={index}
                                className={clsx(
                                    `py-[1rem] break-words text-[13px] text-center cursor-pointer lg:min-w-full hover:text-[#546e7a] lg:w-full w-[25%]`,
                                    tab === tabItem.value && "bg-[#3f4652] text-[#eee] lg:border-l-2 border-b-2 lg:border-b-0 border-mainColor"
                                )}
                                onClick={() => changeTab(tabItem.value)}
                            >
                                <div>
                                   <FontAwesomeIcon
                                       icon={["fas", tabItem.icon as IconProp] as IconProp}
                                       className={"w-full block text-[22px]"}
                                   />
                                    <span>
                                         {tabItem.label}
                                    </span>
                                </div>
                            </div>)}
                    </div>
                </div>
                {tab !== "preview" &&
                    <div className={"w-full relative max-h-[calc(100vh_-_68px)] bg-[#3f4652] overflow-y-auto overflow-x-hidden"}>
                        {tab === "info" &&
                            <div>
                                <InfoComponent
                                    size={size}
                                    changeColorShirt={changeColorShirt}
                                    activeColor={activeColor}
                                    chooseSize={chooseSize}
                                    setQuantity={setQuantity}
                                    quantity={quantity}
                                    productType={productType}
                                    setProduct={setProduct}
                                    setLoaderProduct={setLoaderProduct}
                                />
                                <div className={"w-full"}>
                                    <div className={"text-mainColor text-center border-b-[1px] border-t-[1px] border-[#484f5a] p-3"}>
                                        Description
                                    </div>
                                    <div className={"text-center p-[1rem] text-[17px] no-highlight text-white"}>
                                        <p className={"block"}>
                                            100% cotton
                                        </p>
                                        <p className={"block"}>
                                            White: 160 g/m²
                                        </p>
                                        <p className={"block"}>
                                            Color: 165 g/m²
                                        </p>
                                    </div>
                                </div>
                            </div>
                        }
                        {tab === "text" &&
                            <TextComponent
                                setSelectShape={setSelectShape}
                                changeColorText={changeColorText}
                                activeColor={activeColor}
                                text={text}
                                setText={setText}
                                selectShape={selectShape}
                            />
                        }
                        {tab === "image" &&
                            <ImageComponent
                                handleFileDelete={handleFileDelete}
                                setSelectShape={setSelectShape}
                                imageUpload={imageUpload}
                                inputRef={inputRef}
                                handleFileUpload={handleFileUpload}
                                logoEffects={logoEffects}
                                changeEffect={changeEffect}
                                selectShape={selectShape}
                            />
                        }
                        {tab === "notes" &&
                            <NotesComponent setNotes={setNotes} notes={notes} />
                        }
                        {/*<button onClick={handleExport}>Screen</button>*/}
                    </div>
                }
            </div>
        </div>
    )
}