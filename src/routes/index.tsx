import {Outlet, Route, Routes} from "react-router-dom";
import { lazyImport } from "../utils/lazyImport";
import {Layout} from "../components/Layout";
import {DesignSticker} from "../features/sticker";
import {Cart} from "../features/cart";

const { Home } = lazyImport(() => import("../features/home"), "Home");
const { DesignShirt } = lazyImport(() => import("../features/shirt"), "DesignShirt");
const MainLayout = () => {
    return (
        <Layout>
            <Outlet />
        </Layout>
    );
};

export const AppRoutes = () => {

    return (
        <Routes>
            <Route path="" element={<MainLayout />}>
                <Route path="/" element={<Home />} />
                <Route path="/design-shirt" element={<DesignShirt />} />
                <Route path="/design-sticker" element={<DesignSticker />} />
                <Route path="/checkout" element={<Cart />} />
            </Route>
        </Routes>
    );
};
