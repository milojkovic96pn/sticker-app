import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { ErrorBoundary } from "react-error-boundary";

type ProviderProps = {
    children: React.ReactNode;
};
const ErrorFallback = () => {
    return (
        <div
            className="text-red-500 w-screen h-screen flex flex-col justify-center items-center"
            role="alert"
        >
            <h2 className="text-lg font-semibold">Ooops, something went wrong :( </h2>
            <button
                className="mt-4"
                onClick={() => window.location.assign(window.location.origin)}
            >
                Refresh
            </button>
        </div>
    );
};

const errorHandler = (error: Error) => {
    console.log("from error boundary: ", error);
};

const spinner =  <div
    className={"middle-absolute-front flex items-center justify-center relative overflow-hidden"}
>
    <div className="lds-ring lds-ring-shirt">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
export const AppProvider = ({ children }: ProviderProps) => {

    return (
        <React.Suspense fallback={spinner}>
            <ErrorBoundary FallbackComponent={ErrorFallback} onError={errorHandler}>
                <Router>{children}</Router>
            </ErrorBoundary>
        </React.Suspense>
    );
};
