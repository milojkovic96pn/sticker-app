import create from "zustand";
import { persist, devtools } from "zustand/middleware";

type Props = {
    cart: any[];
    addToCart : (value: any) => void;
}
export const cartStore = create<Props>((set) => ({
    cart: [],
    addToCart : (value: any) => {
        set((state) => ({
            cart: [
                ...state.cart,
                value,
            ],
        }));
    },
}));
