import { library } from "@fortawesome/fontawesome-svg-core";

import {
  faArrowRotateLeft, faArrowRotateRight,
  faBars,
  faCheck, faCircle,
  faClock, faComment,
  faEnvelope, faFileLines, faFont, faImage, faInfo, faLayerGroup,
  faLocationDot, faMinus, faPaintbrush, faPhone, faPlus, faScissors, faShoppingCart, faTimes, faTrash,faShirt,faEdit,faGear,faQuestion,faCartShopping,
  faX

} from "@fortawesome/free-solid-svg-icons";

library.add(
  faLocationDot,faEnvelope,faPhone,faClock,faInfo,faFont,faImage,faFileLines,faCheck,faPlus,faMinus,faTimes,faScissors,faPaintbrush,
    faCircle,faShoppingCart,faTrash,faComment,faBars,faLayerGroup,faArrowRotateLeft,faArrowRotateRight,faShirt,faEdit,faGear,faQuestion,faCartShopping,faX);
