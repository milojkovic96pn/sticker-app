import Select, { components } from "react-select";
import clsx from "clsx";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

type Props = {
    label?: string;
    errorText?: boolean;
    options: any;
    classNames?: string;
    defaultValueProp?: any;
    placeHolder?: string;
    handleChangedValue?: (e: any) => void;
    handleSearchedValue?: (e: string) => void;
    noOptionsText?: string;
    registration?: any;
    type?: "category";
    defaultValue?: any;
    value: string | number;
    onChange: (value: any) => void;
    isClearable?: boolean;
    padding?: number;
    disabled?: boolean;
    customPropsStyle?: any;
};

type ValueType = {
    label: string;
    value: number;
}[];

export const ReactSelect = ({
                                onChange,
                                value,
                                errorText,
                                options,
                                label,
                                placeHolder,
                                classNames,
                                handleSearchedValue,
                                noOptionsText,
                                disabled = false,
                                isClearable = true,
                            }: Props) => {
    const customStyles = {
        control: (styles: any) => ({
            ...styles,
            border: errorText ? "1px solid red" : 0,
            backgroundColor: "#323844",
            outline: "none",
            boxShadow: "none",
            borderRadius: "5px",
            width: "200px"
        }),
        indicatorSeparator: () => ({
            display: "none",
        }),
        dropdownIndicator: (base: any) => ({
            ...base,
            color: "#ddd",
            paddingTop: 0,
            paddingBottom: 0,
            width: 32,
        }),
        groupHeading: () => ({
            fontSize: 16,
            paddingLeft: 5,
            fontWeight: "bold",
        }),
        ClearIndicator: (...props: any) => ({
            ...props,
            cursor: "pointer",
        }),
        valueContainer: (styles: any) => ({
            ...styles,
            cursor: "pointer"
        }),
        indicatorsContainer: (styles: any) => ({
            ...styles,
            cursor: "pointer",
        }),
        clearIndicator: (styles: any) => ({
            ...styles,
            padding: "0px",
        }),
        menu: (styles: any) => ({
            ...styles,
        }),
        singleValue: (styles: any) => ({
            ...styles,
            color: "#ddd"
        }),
        option: (styles: any, { isSelected, isDisabled }: any) => ({
            ...styles,
            backgroundColor: isSelected ?  "#EBD303" :"",
            "&:hover": {
                color: "black",
                cursor: isDisabled ? "default" : "pointer",
                background: "#ddd"
            },
            "&:active": {
                background: "none"
            },
            textAlign: isDisabled && "center"
        }),
    };

    const ClearIndicator = (props: any) => {
        const { getStyles } = props;
        return (
            <div
                style={getStyles("clearIndicator", props)}
                className="cursor-pointer"
            >
                <div style={{ padding: "0px 5px" }}>
                    <FontAwesomeIcon
                        onClick={() => {
                            props.clearValue();
                        }}
                        icon={["fas", "times"]}
                        className=""
                    />
                </div>
            </div>
        );
    };

    const NoOptionsMessage = (props: any) => {
        return (
            <components.NoOptionsMessage {...props}>
        <span className="custom-css-class">
          {noOptionsText && noOptionsText}
        </span>
            </components.NoOptionsMessage>
        );
    };

    return (
        <div className={clsx(classNames && classNames)}>
            {label && (
                <label className="text-[18px] text-[#333333] block font-dmSans pb-1 mt-2  dark:text-white">
                    {label}
                </label>
            )}

            <Select
                styles={customStyles}
                components={{
                    ClearIndicator: (props: any) => ClearIndicator({ ...props }),
                    NoOptionsMessage,
                }}
                value={options.find((c: any) => {
                    return c.value === value;
                }) ?
                    options.find((c: any) => {
                        return c.value === value;
                    }) : ""
                  }
                onChange={(val) => val === undefined ? "" : onChange(val?.value)}
                isClearable={isClearable}
                onInputChange={(e: any) => {
                    handleSearchedValue && handleSearchedValue(e);
                }}
                options={options}
                placeholder={placeHolder || "Select"}
                className={clsx("rounded-[0.5rem]")}
                isDisabled={disabled}
                isSearchable={false}
            />
        </div>
    );
};
