import React, { FC } from 'react';
import "./toggle.css";

export const Toggle:FC<{leftComponent: React.ReactNode, rightComponent: React.ReactNode, value: boolean, [key:string]:any}> = ({value, leftComponent, rightComponent, ...props}) => {

    return (
        <div className={`vo-toggle-main lg:mr-[15px] ${value ? 'vo-toggle-main-active' : ''}`} {...props}>
            <div className="vo-toggle-icons">
                <div className={`vo-toggle-icon ${!value ? 'vo-toggle-icon-hide' : ''}`}>
                    {leftComponent}
                </div>
                <div className={`vo-toggle-icon ${value ? 'vo-toggle-icon-hide' : ''}`}>
                    {rightComponent}
                </div>
            </div>
            <div className={`vo-toggle-dot`}></div>
        </div>
    )
}