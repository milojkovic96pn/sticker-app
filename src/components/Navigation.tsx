import clsx from "clsx";
import {NavLink} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import logo from "../assets/images/logo.png"
import {useEffect, useRef, useState} from "react";
import {useClickAway} from "react-use";
import {cartStore} from "../stores/cart";
import {formatPrice} from "../helpers/formatPrice";
export const Navigation = () => {

    const { cart } = cartStore()
    let price = cart?.reduce(function (acc: any, obj: any) {
        return acc + obj.price;
    }, 0)
    const [collapse, setCollapse] = useState(false);
    const ref = useRef(null);

    const handleCollapse = () => {
        setCollapse(!collapse);
    };

    useClickAway(ref, () => {
        if (collapse) {
            setTimeout(() => {
                handleCollapse();
            }, 100);
        }
    });

    return (
       <div className={"flex lg:justify-center items-center justify-between lg:gap-0 bg-[#ebd303]"}>
           <div
               className="pl-1 lg:pl-0 lg:absolute lg:top-2 lg:max-w-[150px] max-w-[70px] z-[99] lg:left-20 left-1 top-1 flex justify-center lg:bg-transparent bg-[#ebd303] items-center"
           >
               <NavLink
                   to={"/"}
               >
                   <img src={logo} alt={"logo"} />
               </NavLink>
           </div>
           <nav
               className={clsx(
                   `py-3 px-2 z-50 w-full flex-wrap items-center justify-between bg-[#ebd303] lg:pl-[16rem] lg:block hidden`,
               )}
           >
               <div
                   className=" mx-auto flex flex-wrap items-center justify-center"
               >
                   <div
                       className="lg:flex items-center bg-white lg:bg-transparent lg:shadow-none"
                   >
                       <ul className="flex flex-col lg:flex-row list-none lg:ml-auto">
                           <li className="flex items-center">
                               <NavLink
                                   to={"/"}
                                   className="lg:hover:opacity-[0.7] text-[#222222] px-3 py-4 lg:py-2 flex items-center text-lg uppercase font-bold cursor-pointer"
                               >
                                   Home
                               </NavLink>
                           </li>
                           <li className="flex items-center">
                            <span
                                className="lg:hover:opacity-[0.7] text-[#222222] px-3 py-4 lg:py-2 flex items-center text-lg uppercase font-bold cursor-pointer"
                            >
                                About
                            </span>
                           </li>
                           <li className="flex items-center">
                            <span
                                className="lg:hover:opacity-[0.7] text-[#222222] px-3 py-4 lg:py-2 flex items-center text-lg uppercase font-bold cursor-pointer"
                            >
                                Contact
                            </span>
                           </li>
                       </ul>
                   </div>
               </div>
           </nav>
           <div
               className="text-center flex justify-center items-center lg:px-5 px-0.5 md:gap-10 gap-7 cursor-pointer no-highlight bg-[#ebd303]"
           >
              <div className={"md:items-center md:justify-center md:gap-5 lg:gap-3 flex lg:mr-[12px] ml-2 lg:ml-0 gap-2"}>
                  <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="mx-auto h-full lg:w-8 w-6"
                      fill="currentColor"
                      viewBox="0 0 24 24">
                      <path d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069
                        4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204
                         0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227
                          1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059
                           1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014
                            4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0
                            5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4
                             1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z"
                      />
                  </svg>
                  <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="mx-auto h-full lg:w-8 w-6"
                      fill="currentColor"
                      viewBox="0 0 24 24">
                      <path
                          d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"/>
                  </svg>
              </div>
           </div>
           <div
               className="text-center flex justify-center items-center md:px-2 md:gap-10 gap-7 no-highlight bg-[#ebd303] lg:w-[200px]"
           >
               <NavLink
                   to={"/checkout"}
                   className={clsx(
                       ``,
                       price === 0 ? "pointer-events-none" : "cursor-pointer"
                   )}
               >
                   <div className={"flex gap-3 mr-[12px]"}>
                     <span className={"font-bold text-[22px]"}>
                       {formatPrice(price, "USD")}
                     </span>
                         <span className={"w-full text-[22px]"}>
                         <FontAwesomeIcon
                             icon={["fas", "shopping-cart"]}
                         />
                     </span>
                   </div>
               </NavLink>
           </div>
           <div
               className="pr-1 lg:pr-0 text-center flex justify-center items-center lg:px-5 md:gap-10 gap-7 cursor-pointer no-highlight bg-[#ebd303] lg:hidden"
           >
             <span className={"w-full text-[22px] block"} ref={ref} onClick={() => handleCollapse()} >
                  <FontAwesomeIcon
                      icon={["fas", "bars"]}
                  />
             </span>
           </div>
           {collapse &&
               <div className="block lg:flex flex-grow items-center lg:bg-transparent lg:shadow-none md:hidden absolute top-[37px] w-full z-[99] bg-mainColor border-b-black"
                    id="example-collapse-navbar">
                   <ul className="flex flex-col lg:flex-row list-none mr-auto">
                       <li className="flex items-center">
                           <NavLink
                               to={"/"}
                               className="lg:text-white lg:hover:text-gray-300 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                           >
                               Home
                           </NavLink>
                       </li>
                       <li className="flex items-center">
                           <NavLink
                               to={"/about"}
                               className="lg:text-white lg:hover:text-gray-300 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                           >
                               About
                           </NavLink>
                       </li>
                       <li className="flex items-center">
                           <NavLink
                               to={"/contact"}
                               className="lg:text-white lg:hover:text-gray-300 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                           >
                               Contact
                           </NavLink>
                       </li>
                   </ul>
               </div>
           }
       </div>
    )
}