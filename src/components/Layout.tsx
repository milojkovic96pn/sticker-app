import React, {FunctionComponent} from "react";
import {Navigation} from "./Navigation";

type LayoutProps = {
    children: React.ReactNode;
};

export const Layout:FunctionComponent<LayoutProps> = props => {

    const { children } = props

    return (
       <>
           <Navigation />
           <main className={""}>
               {children}
           </main>
       </>
    );
};
