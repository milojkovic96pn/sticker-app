import React, {FunctionComponent, useRef} from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useClickAway } from "react-use";
import clsx from "clsx";

export type ModalProps = {
    show: boolean;
    children: React.ReactNode;
    className?: string;
    close?: () => void;
    typeModal?: "center" | "side" | "top";
    title?: string | React.ReactNode;
    loading?: boolean
};

const ModalContainer = styled.div<{ show: any,typemodal:string }>`
  right: ${(props) => (props.show ? "0" : "-100%")};
  transition: ${(props) => (props.typemodal === "center" ? "center 0.4s" : "right 0.2s")};
  &::-webkit-scrollbar {
    width: 6px;
    height: 6px;
  }

  &::-webkit-scrollbar-track {
    background: #0000000f;
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 20px;
    border: 1px solid #0000000f;
    background-color: #999999;
  }
`;

const Wrapper = styled.div<{ show: any }>`
  right: ${(props) => (props.show ? "0" : "-100%")};
  opacity: ${(props) => (props.show ? "100" : "0")};
  transition: right 0.2s;
  transition: opacity 0.2s;
`;

export const Modal : FunctionComponent<ModalProps> = props => {

    const { loading = false,show, close, children, className, typeModal = "side", title } = props
    const ref = useRef(null);

    // useClickAway(ref, () => {
    //     if (show && close) {
    //         setTimeout(() => {
    //             close();
    //         }, 200);
    //     }
    // });

    return (
        <Wrapper
            className={clsx(
                `fixed w-screen h-screen top-0 bg-gray-700 bg-opacity-60 z-[9999] dark:bg-black/70`,
                typeModal === "center" && "flex justify-center items-center"
            )}
            show={show ? 1 : 0}
        >
            <ModalContainer
                ref={ref}
                show={show ? 1 : 0}
                typemodal={typeModal}
                className={clsx(
                    `bg-clip-border break-words flex flex-col side-modal w-full md:w-1/2 top-0 bg-white z-90 
                    pb-5  shadow-3xl dark:bg-slate-850`,
                    typeModal === "center" ? "!overflow-unset mx-auto  max-h-[95vh] max-w-[95%] relative p-5" :
                        typeModal === "top" ? "!block rounded-0 !w-full pt-5 overflow-y-auto overflow-x-hidden" :
                            "fixed h-screen  px-2.5 overflow-y-auto overflow-x-hidden",
                    className && className
                )}
            >
                {close && !loading &&
                    <div
                        className={clsx(
                            `hover:opacity-90 absolute bg-mainColor rounded-[50%] flex items-center
                    justify-center w-[25px] h-[25px] text-white cursor-pointer dark:bg-darkMainColor`,
                            typeModal === "center" ? "lg:top-[-25px] lg:right-[-30px] top-[-35px] right-0" : "top-3 right-5",
                        )}
                        onClick={close}
                    >
                        <FontAwesomeIcon
                            className={"w-[15px] h-[15px]"}
                            icon={["fas", "times"]}
                        />
                    </div>
                }
                {title &&
                    <>
                        <div className="px-6 pt-3 pb-0 mb-0 border-b-0 rounded-t-2xl">
                            <div className="float-left">
                                <h5 className="mt-0 mb-0 dark:text-white font-medium text-lg">
                                    {title}
                                </h5>
                            </div>
                        </div>
                        <hr className="pb-[1.5px] h-px mx-0 my-1 bg-transparent bg-gradient-to-r from-transparent via-black/40 to-transparent dark:bg-gradient-to-r dark:from-transparent dark:via-white dark:to-transparent"/>
                    </>
                }
                <div
                    className={clsx(
                        `flex-auto pt-0 overflow-auto sm:pt-0`,
                        typeModal !== "center" && "p-6"
                    )}
                >
                    {show ? children : null}
                </div>
            </ModalContainer>
        </Wrapper>
    );
};
